# 代码生成器
登录数据库，选择表进行代码生成，模板可自定义。
模板应该是特殊的，适合自己的代码处理逻辑的，不是为了共用而存在的，
所以你可以创建自己个性的模板，生成与众不同的代码或文件，或其它的东西，不局限于项目使用的框架


# 技术框架
1. 基于`gradle`创建的`IntelliJ Plugin`
2. 管理`bean`容器使用`intellij idea` 自带的`PicoContainer`
3. 历史遗留问题`swing`组件 `intellij idea`组件混用
4. 表单采用`jformdesigner`设计


# 使用
> 请点击查看[**中文文档**](http://poet.gitee.io/codeauto.intellj-plugin.document)


# 插件安装
可以下载`zip/CodeAuto.IntelliJ-Plugin-1.x.zip`手动安装，适应版本范围2016.1~2017.2.*
> 插件已通过官方审核，也可以在**`Intellij`**中直接搜索**`CodeAuto`**

# 版本日志

##### [v1.0] 2017.09.01
- 由[CodeAuto](https://gitee.com/poet/CodeAuto)演化而来

##### [v1.1] 2017.11.01
- 删除了历史遗留的`spring`代码
- 删除了spring的相关依赖，采用`PicoContainer`作为`IOC`
- 修复了模板配置，代码生成页面不显示的`BUG`
- 修复了空模板异常的`BUG`，会有相应的提示
- 修复了代码生成异常的`BUG`

##### [v1.1.10] 2017.11.29
- 修复了数据库切换不成功的BUG

##### [v1.1.11] 2017.11.30
- 增加mysql decimal类型的映射支持

##### [v1.2] 待开发<br>
- 增加默认的模板
- 优化UI显示
- 多数据库连接方案
- 模板配置放入到settings中
- 优化登录方案

# 参考文档
1. `IntelliJ-Plugin` 官方插件开发文档 http://www.jetbrains.org/intellij/sdk/docs/tutorials.html
2. `Gradle`支持 http://www.jetbrains.org/intellij/sdk/docs/tutorials/build_system/prerequisites.html
3. `PicoContainer` 官方文档 http://picocontainer.com/
4. `Intellij 组件示例` https://www.programcreek.com/java-api-examples/index.php?api=com.intellij.openapi.ui.DialogWrapper
5. `moxun's blog` https://moxun.me/archives/category/%E9%BB%91%E7%A7%91%E6%8A%80/idea%E6%8F%92%E4%BB%B6%E5%BC%80%E5%8F%91
6. `gradle-intellij-plugin`插件地址 https://github.com/JetBrains/gradle-intellij-plugin
7. `jfromdesiger` http://www.formdev.com/jformdesigner/