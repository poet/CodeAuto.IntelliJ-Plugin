package com.seven;

import com.intellij.openapi.actionSystem.ActionManager;
import com.intellij.openapi.actionSystem.DefaultActionGroup;
import com.intellij.openapi.components.ApplicationComponent;
import com.seven.action.CodeCreateAction;
import com.seven.action.TemplateConfigAction;
import com.seven.ioc.CodeAutoManager;
import org.jetbrains.annotations.NotNull;

/**
 * @author seven
 */
public class MyPluginRegistration implements ApplicationComponent {

    // Returns the component name (any unique string value).
    @Override
    @NotNull
    public String getComponentName() {
        return "MyPlugin.CodeAuto";
    }


    // If you register the MyPluginRegistration class in the <application-components> section of
    // the plugin.xml file, this method is called on IDEA start-up.
    @Override
    public void initComponent() {
        ActionManager am = ActionManager.getInstance();

        CodeAutoManager.instance();

        CodeCreateAction codeCreateAction = CodeAutoManager.getBean(CodeCreateAction.class);
        TemplateConfigAction templateConfigAction = CodeAutoManager.getBean(TemplateConfigAction.class);

        am.registerAction("Myplugin.active", codeCreateAction);
        am.registerAction("Myplugin.config", templateConfigAction);

        DefaultActionGroup windowM = (DefaultActionGroup) am.getAction("WindowMenu");
        windowM.addSeparator();
        windowM.add(codeCreateAction);
        windowM.add(templateConfigAction);

    }

    // Disposes system resources.
    @Override
    public void disposeComponent() {
        CodeAutoManager.dispose();
    }
}
