package com.seven.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogBuilder;
import com.intellij.openapi.util.IconLoader;
import com.seven.ioc.CodeAutoManager;
import com.seven.swing.component.dialog.MyDialogBuilder;
import com.seven.swing.component.form.CreateForm;
import com.seven.swing.component.form.LoginForm;

/**
 * @author seven
 */
public class CodeCreateAction extends AnAction {


    public CodeCreateAction(TemplateConfigAction action) {
        super("代码生成","连接数据库 -> 选择表 -> 选择模板 + 其它配置 -> 生成代码", IconLoader.getIcon("/images/logo/code.png"));
    }

    @Override
    public void actionPerformed(AnActionEvent event) {
        CodeAutoManager.PROJECT = event.getData(PlatformDataKeys.PROJECT);
        MyDialogBuilder.showLoginDialog();
    }


}
