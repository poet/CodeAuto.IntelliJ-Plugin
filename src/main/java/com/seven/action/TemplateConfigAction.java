package com.seven.action;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.util.IconLoader;
import com.seven.ioc.CodeAutoManager;
import com.seven.swing.component.dialog.MyDialogBuilder;

/**
 * @author seven
 */
public class TemplateConfigAction extends AnAction {

    public TemplateConfigAction() {
        super("模板配置","自定义路径、自定义结构、自定义文件格式、自行组织文本格式", IconLoader.getIcon("/images/logo/template.png"));
    }

    @Override
    public void actionPerformed(AnActionEvent event) {
        CodeAutoManager.PROJECT = event.getData(PlatformDataKeys.PROJECT);
        MyDialogBuilder.showTemplateDialog();
    }
}
