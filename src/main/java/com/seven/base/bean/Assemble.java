package com.seven.base.bean;

import java.util.List;

/**
 * 封装了所有的配置信息
 * 
 * @author deyang
 *
 */
public class Assemble {

	// 作者
	private String author;
	// 模板名称
	private String template;
	// 模块名称
	private String module;
	// 生成路径
	private String codepath;
	// 选中的表元素
	private List<Item> items;
	
	public Assemble() {
		super();
	}
	public Assemble(String author, String module, String template,
			String codepath) {
		this.author = author;
		this.module = module;
		this.template = template;
		this.codepath = codepath;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getCodepath() {
		return codepath;
	}
	public void setCodepath(String codepath) {
		this.codepath = codepath;
	}
	public List<Item> getItems() {
		return items;
	}
	public void setItems(List<Item> items) {
		this.items = items;
	}
	
}
