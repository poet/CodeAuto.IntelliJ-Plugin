package com.seven.base.bean;

import com.seven.base.util.PropertyUtil;
import com.seven.base.util.StringUtil;

import java.util.Map;

/**
 * 数据库表中的字段
 * 
 * @author deyang
 *
 */
public class Field {
	// 是否主键
	private boolean pri = false;
	
	// 字段名称
	private String name;
	
	// 注释
	private String comment;
	
	// 字段类型
	private String type;
	
	// 字段长度
	private int length;
	
	// 类中字段的名称
	private String javaName;
	
	// 字段对应的java类型, 比如Integer, Long等类型, 每个类型呈现不同的样式
	private String javaType;
	
	// 持久化层类型，比如mybatis, hibernate对应的类型
	private String ormType;
	
	// 表单lable
	private String htmlLabel;
	
	// 共有input,time,select,radio,checkbox,editor6大类型
	private String htmlType; 
	
	// 只有是select,radio,checkbox有内容，存的是Map<key,value>
	private Map<String, String> htmlMap;
	
	// 查询条件或判断条件
	private String condition = "";
	
	public Field() {
		super();
	}
	
	public Field(boolean pri, String name, String comment, String type) {
		super();
		this.pri = pri;
		this.name = name;
		this.comment = comment;
		this.type = type;
	}
	
	public boolean isPri() {
		return pri;
	}
	public void setPri(boolean pri) {
		this.pri = pri;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public String getJavaName() {
		return javaName;
	}

	public void setJavaName(String javaName) {
		this.javaName = javaName;
	}

	public String getJavaType() {
		return javaType;
	}

	public void setJavaType(String javaType) {
		this.javaType = javaType;
	}

	public String getHtmlLabel() {
		return htmlLabel;
	}

	public String getOrmType() {
		return ormType;
	}

	public void setOrmType(String ormType) {
		this.ormType = ormType;
	}

	public void setHtmlLabel(String htmlLabel) {
		this.htmlLabel = htmlLabel;
	}

	public String getHtmlType() {
		return htmlType;
	}

	public void setHtmlType(String htmlType) {
		this.htmlType = htmlType;
	}

	public Map<String, String> getHtmlMap() {
		return htmlMap;
	}

	public void setHtmlMap(Map<String, String> htmlMap) {
		this.htmlMap = htmlMap;
	}

	public String getCondition() {
		return condition;
	}

	/**
	 * 设置条件
	 * 
	 * @param condition
	 */
	public void setCondition(String condition) {
		this.condition = condition;
	}

	public Field comb() {
		this.javaName = StringUtil.convertToJavaField(name);
		this.javaType = PropertyUtil.get((PropertyUtil.STR_JAVA + type).toLowerCase());
		this.ormType = PropertyUtil.get((PropertyUtil.STR_ORM + type).toLowerCase());
		this.htmlLabel = StringUtil.htmlLabel(name, comment);
		this.htmlType = StringUtil.htmlType(type, comment);
		this.htmlMap = StringUtil.htmlMap(comment);
		return this;
	}
	
}
