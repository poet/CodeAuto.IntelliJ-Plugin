package com.seven.base.bean;

import java.util.List;

/**
 * 选中的代码配置元素
 * 
 * @author deyang
 *
 */
public class Item {
	
	// 选中的表
	private Table tb;
	// 编辑字段
	private List<Field> listEdit;
	// 搜索字段
	private List<Field> listSearch;
	// 列表显示字段
	private List<Field> listPage;
	
	public Table getTb() {
		return tb;
	}
	public void setTb(Table tb) {
		this.tb = tb;
	}
	public List<Field> getListEdit() {
		return listEdit;
	}
	public void setListEdit(List<Field> listEdit) {
		this.listEdit = listEdit;
	}
	public List<Field> getListSearch() {
		return listSearch;
	}
	public void setListSearch(List<Field> listSearch) {
		this.listSearch = listSearch;
	}
	public List<Field> getListPage() {
		return listPage;
	}
	public void setListPage(List<Field> listPage) {
		this.listPage = listPage;
	}

}
