package com.seven.base.bean;

import java.util.List;


/**
 * 数据库表
 * 
 * @author deyang
 *
 */
public class Table {

	// 表名
	private String name;
	// 描述
	private String comment;
	// 类名
	private String Entity;
	
	// 主键信息
	private Field pk;
	
	// 字段信息
	private List<Field> fds;

	public Table() {
		super();
	}

	public Table(String name, String comment) {
		super();
		this.name = name;
		this.comment = comment;
	}
	
	public Table(String name, String comment, String Entity) {
		super();
		this.name = name;
		this.comment = comment;
		this.Entity = Entity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<Field> getFds() {
		return fds;
	}

	/**
	 * 设置所有字段
	 * @param fds
	 */
	public void setFds(List<Field> fds) {
		this.fds = fds;
		// 找出主键
		for (Field fd : fds) {
			if (fd.isPri()) {
				this.pk = fd;
				break;
			}
		}
		if (this.pk == null) {
			throw new RuntimeException(name + "这张表没有设置主键~");
		}
		fds.remove(this.pk);// 从列表中删除主键的信息
	}

	public String getEntity() {
		return Entity;
	}

	public void setEntity(String entity) {
		Entity = entity;
	}

	public Field getPk() {
		return pk;
	}

	public void setPk(Field pk) {
		this.pk = pk;
	}


}
