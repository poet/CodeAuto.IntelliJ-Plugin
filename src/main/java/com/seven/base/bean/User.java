package com.seven.base.bean;

import org.apache.commons.lang3.StringUtils;

public class User {
	
	// 账号
	private String username;
	// 密码
	private String password;
	// 数据连接URL
	private String jdbcUrl;
	// 数据库方言
	private String dialect = "mysql";
	
	public User() {
		super();
	}
	
	public User(String dialect, String jdbcUrl, String username, String password) {
		super();
		this.dialect = dialect;
		this.jdbcUrl = jdbcUrl;
		this.username = username;
		this.password = password;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getJdbcUrl() {
		return jdbcUrl;
	}
	public void setJdbcUrl(String jdbcUrl) {
		this.jdbcUrl = jdbcUrl;
	}
	public String getDialect() {
		return dialect;
	}
	public void setDialect(String dialect) {
		this.dialect = dialect;
	}

	/**
	 * 输入的登录信息是否完整
	 * 
	 * @return 
	 */
	public boolean isWhole() {
		return !isNull(username, password, jdbcUrl, dialect);
	}
	
	// 判断传入的值
	private boolean isNull(String... texts) {
		for (String text : texts) {
			if (StringUtils.isBlank(text)) {
				return true;
			}
		}
		return false;
	}
}
