package com.seven.base.dao;

import com.seven.base.bean.Field;
import com.seven.base.bean.Table;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 * 抽象类，作为中间层
 * 
 * @author deyang
 *
 */
public abstract class AbstractQueryDao extends JdbcTemplate implements QueryDao{

	// 表映射器
	RowMapper<Table> tableRowMapper;
	// 字段映射器
	RowMapper<Field> fieldRowMapper;
	
	@Override
	public void afterPropertiesSet() {
		// 这里什么也不做, 避免抛出没有设置数据源异常
		// 数据源初始化推迟
	}

	@Override
    public void setTableRowMapper(RowMapper<Table> tableRowMapper) {
		this.tableRowMapper = tableRowMapper;
	}

	@Override
    public void setFieldRowMapper(RowMapper<Field> fieldRowMapper) {
		this.fieldRowMapper = fieldRowMapper;
	}

}
