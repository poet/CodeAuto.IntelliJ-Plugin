package com.seven.base.dao;

import com.seven.base.bean.Field;
import com.seven.base.bean.Table;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

/**
 * 数据库查询
 * 
 * @author deyang
 *
 */
public interface QueryDao {
	
	/**
	 * 设置数据源
	 * 
	 * @param dataSource 数据源
	 */
	public void setDataSource(DataSource dataSource);
	
	/**
	 * 查询数据中的所有表信息
	 * 
	 * @return
	 */
	public List<Table> queryTables();
	
	/**
	 * 查询表的所有字段信息
	 * 
	 * @param tbName 表名
	 * @return
	 */
	public List<Field> queryFields(String tbName);
	
	/**
	 * 设置查询表的映射对象
	 * 
	 * @param tableRowMapper
	 */
	public void setTableRowMapper(RowMapper<Table> tableRowMapper);
	/**
	 * 设置查询字段的映射对象
	 * 
	 * @param fieldRowMapper
	 */
	public void setFieldRowMapper(RowMapper<Field> fieldRowMapper);
}
