package com.seven.base.dao;

import com.seven.base.bean.Field;
import com.seven.base.bean.Table;
import com.seven.base.util.PropertyUtil;

import java.util.List;

public class QueryDaoImpl extends AbstractQueryDao {

	@Override
	public List<Table> queryTables() {
		return this.query(PropertyUtil.get("sql.tables"), this.tableRowMapper, PropertyUtil.get("db.name"));
	}

	@Override
	public List<Field> queryFields(String tbName) {
		return this.query(PropertyUtil.get("sql.fields"), this.fieldRowMapper, tbName, PropertyUtil.get("db.name"));
	}
	
}
