package com.seven.base.mapper.mysql;

import com.seven.base.bean.Field;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FieldMapper implements RowMapper<Field> {

	@Override
	public Field mapRow(ResultSet rs, int rowNum) throws SQLException {
		Field field = new Field();
		// TODO 这里主键的判断只对mysql有用, 其它的数据库待以后扩展
		field.setPri("pri".equalsIgnoreCase(rs.getString("pri")));
		field.setName(rs.getString("name"));
		field.setComment(rs.getString("comment"));
		field.setType(rs.getString("type"));
		return field;
	}

}
