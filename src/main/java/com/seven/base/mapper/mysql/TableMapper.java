package com.seven.base.mapper.mysql;

import com.seven.base.bean.Table;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class TableMapper implements RowMapper<Table> {
	
	@Override
	public Table mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Table(rs.getString("name"), rs.getString("comment"));
	}
	
}
