package com.seven.base.service;

import com.seven.base.bean.Assemble;

import java.util.List;

public interface QueryService {

	/**
	 * 取得所有的表名
	 * 
	 * @return
	 */
	String[] queryTbNames();

	/**
	 * 根据表名取得表详细的信息
	 * 
	 * @param tbNames
	 * @return
	 */
	List<Object[]> tableRowDatas(String[] tbNames);

	/**
	 * 查询表所有的字段名称集合
	 * @param tbName
	 * @return
	 */
	String[] queryFdNames(String tbName);

	/**
	 * 根据字段名称取得字段的详细信息
	 * 
	 * @param tbName  表名
	 * @param fdNames 字段名称
	 * @return
	 */
	List<Object[]> fieldEditRowDatas(String tbName, String[] fdNames);

	List<Object[]> fieldSearcjRowDatas(String tbName, String[] fdNames);

	List<Object[]> fieldListRowDatas(String tbName, String[] fdNames);

	void createCode(Assemble assemble);

}
