package com.seven.base.util;

import com.seven.base.bean.Assemble;
import com.seven.base.bean.User;
import org.apache.commons.lang3.time.DateFormatUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * 记录数据库的登录信息
 * 
 * @author deyang
 *
 */
public class Configuration {

	public static final String  USER_HOME = System.getProperty("user.home");
	public static final String ROOT_PATH = USER_HOME + "/.CodeAuto";
	public static final String PROPERTIES_PATH = ROOT_PATH + "/configuration.properties";
	private Properties prop;

	/** 加载配置文件 */
	public static Configuration load() {
		return new Configuration();
	}

	/**
	 * 初始化Configuration类
	 * 
	 *            要读取的配置文件的路径+名称
	 */
	public Configuration() {
		prop = new Properties();
		try {
			File root = new File(ROOT_PATH);
			if (!root.exists()) {
				root.mkdir();
			}
			File file = new File(PROPERTIES_PATH);
			if (!file.exists()) {
				file.createNewFile();
			}
			prop.load(new FileInputStream(file));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	/**
	 * 取得key对应的值
	 * 
	 * @param key
	 * @return key的值
	 */
	public String getValue(String key) {
		if (prop.containsKey(key)) {
            return prop.getProperty(key);// 得到某一属性的值
        } else {
            return null;
        }
	}

	/**
	 * 设置key的值
	 * 
	 * @param key
	 * @param value
	 */
	public void setValue(String key, String value) {
		prop.setProperty(key, value);
	}

	/**
	 * 删除key的值
	 *
	 * @param key
	 */
	public void removeKey(String key) {
		prop.remove(key);
	}


	/**
	 * 将更改后的文件数据存入指定的文件中，该文件可以事先不存在。
	 */
	public void save() {
		FileOutputStream outputFile = null;
		try {
			outputFile = new FileOutputStream(new File(PROPERTIES_PATH));
			prop.store(outputFile, String.format("lastModifyTime : %s", DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss")));
			outputFile.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (outputFile != null) {
					outputFile.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 从配置文件中读取上一次用户登录的用户信息
	 * 
	 * @return 上一次用户登录的信息
	 */
	public static User getHasLoginUser() {
		Configuration fig = load();
		User user = null;
		if (!fig.prop.isEmpty()) {
			user = new User();
			user.setDialect(fig.getValue("db.dialect"));
			user.setUsername(fig.getValue("db.username"));
			user.setPassword(fig.getValue("db.password"));
			user.setJdbcUrl(fig.getValue("db.jdbcUrl"));
		}
		return user;
	}
	
	/**
	 * 取得代码的配置信息，如作者，模板等
	 * 
	 * @return
	 */
	public static Assemble getHasAssemble() {
		Configuration fig = load();
		Assemble assemble = new Assemble();
		if (!fig.prop.isEmpty()) {
			assemble.setAuthor(fig.getValue("ab.author"));
			assemble.setTemplate(fig.getValue("ab.template"));
			assemble.setModule(fig.getValue("ab.module"));
			assemble.setCodepath(fig.getValue("ab.codepath"));
		}
		return assemble;
	}


	public static Map<String, String> getHasTemplates() {
		Map<String, String> map = new HashMap<>();
		Configuration fig = load();
		Set keyValue = fig.prop.keySet();
		String key;
		String value;
		for (Iterator it = keyValue.iterator(); it.hasNext();){
			key = (String) it.next();
			if (key.startsWith("template.")) {
				value = fig.getValue(key);
				map.put(key.replace("template.", ""), value);
			}
		}
		return map;
	}

	/**
	 * 登录成功后将登录的用户信息保存到配置文件中
	 * 
	 * @param user
	 *            登录成功的用户信息
	 */
	public static void saveLoginUser(User user) {
		Configuration fig = load();
		fig.setValue("db.dialect", user.getDialect());
		fig.setValue("db.jdbcUrl", user.getJdbcUrl());
		fig.setValue("db.username", user.getUsername());
		fig.setValue("db.password", user.getPassword());
		fig.save();
	}

	/**
	 * 保存代码的配置信息
	 * 
	 * @param assemble
	 */
	public static void saveAssemble(Assemble assemble) {
		Configuration fig = load();
		fig.setValue("ab.author", assemble.getAuthor());
		fig.setValue("ab.template", assemble.getTemplate());
		fig.setValue("ab.module", assemble.getModule());
		fig.setValue("ab.codepath", assemble.getCodepath());
		fig.save();
	}

	/**
	 * 保存模板对应在路径
	 *
	 * @param name
	 * @param path
	 */
	public static void saveTemplate(String name, String path) {
		FileUtil.mapTemplate.put(name, path);

		Configuration fig = load();
		fig.setValue("template." + name, path);
		fig.save();
	}

    public static void removeTemplate(String name) {
		FileUtil.mapTemplate.remove(name);
		Configuration fig = load();
		fig.removeKey("template." + name);
		fig.save();
    }
}
