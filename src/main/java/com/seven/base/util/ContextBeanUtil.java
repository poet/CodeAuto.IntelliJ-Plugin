package com.seven.base.util;

import com.jolbox.bonecp.BoneCPDataSource;
import com.seven.base.bean.Field;
import com.seven.base.bean.Table;
import com.seven.base.dao.QueryDao;
import com.seven.ioc.CodeAutoManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.RowMapper;

/**
 * 
 * bean的手动管理工具
 * 
 * @author deyang
 *
 */
public class ContextBeanUtil {

	private static Logger log = LoggerFactory.getLogger(ContextBeanUtil.class);

	/**
	 * 将数据源注入到容器中QueryDao的实现类对象中
	 * 
	 * @param dataSource 数据源
	 * @param dialect 方言
	 */
	@SuppressWarnings("unchecked")
	public static void fill(BoneCPDataSource dataSource, String dialect) {

		QueryDao dao = CodeAutoManager.getBean(QueryDao.class);
		dao.setDataSource(dataSource);
		
		Object obj1 = CodeAutoManager.getBean(dialect + "TableMapper");
		if (obj1 == null) {
			throw new RuntimeException("没有配置" + dialect + "对就应的TableMapper实现类");
		}
		dao.setTableRowMapper((RowMapper<Table>)obj1);
		
		Object obj2 = CodeAutoManager.getBean(dialect + "FieldMapper");
		if (obj2 == null) {
			throw new RuntimeException("没有配置" + dialect + "对就应的FieldMapper实现类");
		}
		dao.setFieldRowMapper((RowMapper<Field>)obj2);
	}
	
	/**
	 * 读取配置文件
	 * 
	 * @param location 文件路径
	 * @return 对应的资源
	 */
	public static Resource getResource (String location) {
		return null;
	}
	
	/**
	 * 读取配置文件
	 * 
	 * @param locationPattern
	 * @return 对应的资源集合
	 */
	public static Resource[] getResources (String locationPattern) {
		return null;
	}


}
