package com.seven.base.util;

import com.intellij.ide.highlighter.HtmlFileType;
import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.ide.highlighter.XmlFileType;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.event.DocumentEvent;
import com.intellij.openapi.editor.event.DocumentListener;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.fileTypes.PlainTextFileType;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.seven.ioc.CodeAutoManager;
import com.seven.swing.component.form.TemplateForm;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.io.File;
import java.util.*;


/**
 * 文件工具类, 主要读取数据的配置信息及模板信息
 * 
 * @author deyang
 *
 */
public class FileUtil {

	public static Map<String, String> mapTemplate = Configuration.getHasTemplates();
	public static List<VirtualFile> changeFiles = new ArrayList<>();

	/**
	 * 取得模板名称信息
	 */
	public static String[] getTemplates() {
		String[] templates = new String[mapTemplate.size()];
		int i = 0;
		for (Map.Entry<String, String> entry : mapTemplate.entrySet()) {
			templates[i++] = entry.getKey();
		}
		return templates;
	}

	/**
	 * 取得数据库信息
	 */
	public static String[] getDatabases() {
		return new String[]{"mysql"};
	}

	/**
	 * 取得图片
	 *
	 * @param imagePath
	 * @return
	 */
	public static ImageIcon getImageIcon(String imagePath) {
		return new ImageIcon(FileUtil.class.getResource(imagePath));
	}

	/**
	 * 取得模板的路径
	 * @param category
	 * @return
	 */
	public static String getTemplateDirectory(String category) {
		return mapTemplate.get(category);
	}

	public static List<File> getHasTemplates() {
		List<File> files = new ArrayList<>();
		for (Map.Entry<String, String> entry : mapTemplate.entrySet()) {
			files.add(new File(entry.getValue()));
		}
		return files;
	}


	/**
	 * 取得文件的内容
	 * @param selectionNode
	 * @return
	 */
	public static Document getFileDocument(DefaultMutableTreeNode selectionNode) {
		TreeNode[] nodes = selectionNode.getPath();
		StringBuffer sb = new StringBuffer();
		for (int i = 0 ; i < nodes.length ; i++) {
			if (i == 0) {
                continue;
            }
			if (i == 1) {
				sb.append(mapTemplate.get(nodes[i].toString()));
				continue;
			}
			sb.append("/").append(nodes[i].toString());
		}
		File file = new File(sb.toString());
		VirtualFile virtualFile = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(file);
		PsiFile psiFile = PsiManager.getInstance(CodeAutoManager.PROJECT).findFile(virtualFile);

		if (psiFile == null) {
            return null;
        }

		Document document = PsiDocumentManager.getInstance(CodeAutoManager.PROJECT).getDocument(psiFile);

		TemplateForm templateForm = CodeAutoManager.getBean("templateForm");

		document.addDocumentListener(new DocumentListener() {
			@Override
			public void documentChanged(DocumentEvent event) {
				changeFiles.add(virtualFile);
				templateForm.getApplyBtn().setEnabled(true);
			}
		});
		return document;
	}

	public static FileType getFileType(DefaultMutableTreeNode selectionNode) {
		String fileName = selectionNode.toString();
		// java
		if (fileName.endsWith(JavaFileType.DOT_DEFAULT_EXTENSION)) {
			return JavaFileType.INSTANCE;
		}
		// html
		if (fileName.endsWith(HtmlFileType.DOT_DEFAULT_EXTENSION)) {
			return HtmlFileType.INSTANCE;
		}
		// XML
		if (fileName.endsWith(XmlFileType.DOT_DEFAULT_EXTENSION)) {
			return XmlFileType.INSTANCE;
		}

		return PlainTextFileType.INSTANCE;
	}

	public static void saveChanges() {
		if (!changeFiles.isEmpty()) {
			for (VirtualFile virtualFile : FileUtil.changeFiles) {
				virtualFile.refresh(false, true);
			}
			changeFiles.clear();
		}
	}
}
