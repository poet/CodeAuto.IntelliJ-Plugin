package com.seven.base.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.TreeMap;


public class StringUtil {
  
   private static Logger logger = LoggerFactory.getLogger(StringUtil.class);

	/**
	 * 首字母小写
	 * @param entity
	 * @return
	 */
	public static String firstCharLower(String entity) {
		return entity.substring(0,1).toLowerCase() + entity.substring(1);
	}
	
	/**
	 * 首字母大写
	 * @param entity
	 * @return
	 */
	public static String firstCharUpper(String entity) {
		return entity.substring(0,1).toUpperCase() + entity.substring(1);
	}
	
	/**
	 * 将数据中的字段转化成java的字段
	 * 规则 ABC_DEF  ->  abcDef
	 * @param dataField
	 * @return
	 */
	public static String convertToJavaField(String dataField) {
		String[] arrayStr = dataField.toLowerCase().split("_");
		StringBuffer sb = new StringBuffer();
		for (String str : arrayStr) {
			sb.append(firstCharUpper(str));
		}
		return firstCharLower(sb.toString());
	}
	
	/**
	 * 将数据表转换成实体类名
	 * @param tbName
	 * @return
	 */
	public static String convertToJavaEntity(String tbName) {
		String[] arrayStr = tbName.toLowerCase().split("_");
		StringBuffer sb = new StringBuffer();
		for (String str : arrayStr) {
			sb.append(firstCharUpper(str));
		}
		return sb.toString();
	}
	
	/**
	 * 将数据库中的字段类型转化成java类的数据类型
	 * @param dataType
	 * @return
	 */
	public static String convertToJavaType(String dataType) {
		String javaType = PropertyUtil.get(dataType);
		if(javaType == null){
			logger.error("请在对应的数据库配置文件下，配置{}对的java类型", dataType);
			throw new RuntimeException();
		}
		return javaType;
	}
	
	public static String toDBC(String input) {
		char[] c = input.toCharArray();
		for (int i = 0; i < c.length; i++) {
			if (c[i] == 12288) {
				c[i] = (char) 32;
				continue;
			}
			if (c[i] > 65280 && c[i] < 65375) {
                c[i] = (char) (c[i] - 65248);
            }
		}
		return new String(c);
	}

	/**
	 * 找到包含{1:亚种,2:欧种,3:东南亚,4:北极，5：南极}
	 * 类的字段串转换成map
	 * @param str
	 * @return
	 */
	public static Map<String, String> convertToMap(String str, String prefix, String suffix) {
		Map<String, String> map = new TreeMap<String, String>();
		try{			
			if(str != null){
				str = toDBC(str);
				str = StringUtils.trimAllWhitespace(str);
				int start = str.indexOf(prefix);
				int end = str.indexOf(suffix);
				if(start != -1 && end != -1){
					str = str.substring(start+1, end);
					String[] s;
					for(String wy : str.split(",")) {
						s = wy.split(":");
						map.put(s[0], s[1]);
					}
					return map;
				}
			}
		}catch(Exception e){
//			e.printStackTrace();
			logger.info(str+"这个注释不太规范");
			return map;
		}
		return map;
	}
	
	public static void main(String[] arg){
		String str = "{1:亚种,2：欧种,3:   东南亚,4:北极，5：南极}";
		str = toDBC(str);
		str = str.replace(" ", "");
		int start = str.indexOf("{");
		int end = str.indexOf("}");
		logger.info(str.substring(start+1, end));
		
		System.out.println(convertToJavaField("SSFSF_SSF"));
	}

	/**
	 * 设置字段的html代码的类型
	 * 
	 * @param type
	 * @param comment
	 * @return
	 */
	public static String htmlType(String type, String comment) {
		String htmlType = PropertyUtil.get((PropertyUtil.STR_HTML + type).toLowerCase());
		if (htmlType != null) {
            return htmlType;
        }
		if (comment == null) {
            return "input";
        }
		if (comment.indexOf("{") != -1) {
			htmlType = "select";
		} else if (comment.indexOf("(") != -1) {
			htmlType = "radio";
		} else if (comment.indexOf("[") != -1) {
			htmlType = "checkbox";
		} else {
			htmlType = "input";
		}
		return htmlType;
	}

	public static String htmlLabel(String name, String comment) {
		if (org.apache.commons.lang3.StringUtils.isBlank(comment)){
			return name;
		}
		int i = comment.indexOf("{");
		if (i > 0) {
            return comment.substring(0, i);
        }
		i = comment.indexOf("(");
		if (i > 0) {
            return comment.substring(0, i);
        }
		i = comment.indexOf("[");
		if (i > 0) {
            return comment.substring(0, i);
        }
		return comment;
	}

	public static Map<String, String> htmlMap(String comment) {
		Map<String, String> map = convertToMap(comment, "{", "}");
		if (map.isEmpty()) {
			map = convertToMap(comment, "(", ")");
		}
		if (map.isEmpty()) {
			map = convertToMap(comment, "[", "]");
		}
		return map;
	}
}
