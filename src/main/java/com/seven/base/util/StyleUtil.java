package com.seven.base.util;

import javax.swing.*;
import java.awt.*;

/**
 * 设置swing的风格
 * 
 * @author deyang
 *
 */
public class StyleUtil {

	// 设置使用windows风格
	public static void setWindowStyle(Component component) {
		try {
//			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			SwingUtilities.updateComponentTreeUI(component);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("初始化windows风格窗口失败");
		}
	}
	
	// TODO 其它风格
	// public ...

}
