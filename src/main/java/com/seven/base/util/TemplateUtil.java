package com.seven.base.util;

import com.seven.base.bean.Item;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * 模板工具类
 * 
 * @author deyang
 *
 */
public class TemplateUtil {

	/**
	 * 根据模板分类取得下面的所有文件
	 * 
	 * @param category
	 * @return
	 */
	public static List<String> getTemplatePaths(String category) {

		List<String> list = new ArrayList<String>();
		File template = new File(FileUtil.getTemplateDirectory(category));
		for (File file : FileUtils.listFiles(template, PropertyUtil.get("template.type").split(","), true)) {
			list.add(file.getPath().substring(file.getPath().indexOf("/"+ category +"/") + category.length() + 1));
		}
		return list;

	}
	
	/**
	 * 取得模板所需要的值
	 * 
	 * @param author 作者
	 * @param module 模块名称
	 * @param Entity pojo类名
	 * @param entity pojo类实例化的变量名
	 * @param item 选中的表的其中一个配置
	 * @return
	 */
	public static Map<String, Object> getDataMap(String author, String module, String Entity, String entity, Item item) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("module", module);
		data.put("author", author);
		data.put("Entity", Entity);
		data.put("entity", entity);
		data.put("tb", item.getTb());
		data.put("listEdit", item.getListEdit());
		data.put("listSearch", item.getListSearch());
		data.put("listPage", item.getListPage());
		return data;
	}

	/**
	 * 取得模板生成的路径
	 * 
	 * @param module 模块名称
	 * @param Entity 类名
	 * @param entity 实例名
	 * @param templatePath 模板路径
	 * @param codepath 生成路径
	 * @return 生的文件路径
	 */
	public static String getTargetPath(String module, String Entity,
			String entity, String templatePath, String codepath) {
		String target = codepath + combPath(templatePath, module, Entity, entity);
		File file = new File(target);
		if (!file.exists()) {
			try {
				file.getParentFile().mkdirs();
				file.createNewFile();
				return target;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return file.getPath();
	}
	
	private static String combPath(String templatePath, String module, String Entity, String entity) {
		templatePath = templatePath.replaceAll("\\$\\{module\\}", module);
		templatePath = templatePath.replaceAll("\\$\\{Entity\\}", Entity);
		templatePath = templatePath.replaceAll("\\$\\{entity\\}", entity);
		return templatePath;
	}

	
}
