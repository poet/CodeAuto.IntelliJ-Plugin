package com.seven.base.util;

import com.jolbox.bonecp.BoneCPDataSource;
import com.seven.base.bean.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/**
 * 登录成功的情况下, 初始化配置件
 * 
 * <p>1.初始化dataSource, 并注入到QueryDaoImpl中
 * <br/>2.初始化PropertyUtil工具类
 * <br/>3.将用户的登录信息保存到user.properties配置文件中,避免下次登录
 * 
 * @author deyang
 *
 */
public class UserUtil {
	
	private static Logger log = LoggerFactory.getLogger(UserUtil.class);

	/**
	 * 用户登录
	 * 
	 * @return true表示登录成功, false表示登录失败
	 */
	public static boolean login(final User user) {
		final SynchronousQueue<Boolean> queue = new SynchronousQueue<>();
		new Thread(() -> {
			boolean bool;
			String jdbcUrl = user.getJdbcUrl();
			String username = user.getUsername();
			String password = user.getPassword();
			String dialect = user.getDialect();
			// 加载数据库配置的信息
			initPropertyUtil(dialect);
			BoneCPDataSource dataSource = initDataSource(jdbcUrl.replace("{", "").replace("}", ""), username, password);
			try {
				// 尝试连接~
				dataSource.getConnection();
				// 登录成功将dataSource注入到QueryDaoImpl中
				ContextBeanUtil.fill(dataSource, dialect);
				// 取得数据库信息并保存起来
				PropertyUtil.saveDbName(jdbcUrl);
				log.info("{}登录成功", username);
				bool = true;
			} catch (Exception e) {
				log.error("数据库[username={}, password={}]登录失败~", username, password);
				log.error(e.getMessage());
				dataSource.close();
				bool = false;
			}
			try {
				queue.clear();
				queue.put(Boolean.valueOf(bool));
			} catch (Exception e) {
				// ignore
			}
		}).start();

		try {
			Boolean b = queue.poll(4000, TimeUnit.MILLISECONDS);
			if (b == null || !b) {
				return false;
			} else {
				return true;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 初始化数据源
	 * 
	 * @param username 账号
	 * @param password 密码
	 * @param jdbcUrl 远程URL
	 * @return 数据源
	 */
	private static BoneCPDataSource initDataSource(String jdbcUrl, String username, String password) {
		BoneCPDataSource bd = new BoneCPDataSource();
		bd.setUsername(username);
		bd.setPassword(password);
		bd.setJdbcUrl(jdbcUrl);
		bd.setDriverClass(PropertyUtil.get("db.driverClass"));
		// TODO 其它的一些设置
		return bd;
	}
	
	/**
	 * 根据数据库方言加载对应的配置文件<br>
	 * 配置文件相关的值通过<code>PropertyUtil.get()</code>
	 * 
	 * @param dialect 方言
	 */
	private static void initPropertyUtil(String dialect) {
		// 配置资源路径
		String location = "/database/" + dialect + ".properties";
		try {
			PropertyUtil.RROP.load(UserUtil.class.getResourceAsStream(location));
			PropertyUtil.assembleType();
		} catch (IOException e) {
			log.error("读取{}出错~", location);
			e.printStackTrace();
		}
	}
	
}
