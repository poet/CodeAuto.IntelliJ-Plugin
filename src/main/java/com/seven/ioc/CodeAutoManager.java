package com.seven.ioc;

import com.intellij.openapi.project.Project;
import com.seven.action.CodeCreateAction;
import com.seven.action.TemplateConfigAction;
import com.seven.base.dao.QueryDaoImpl;
import com.seven.base.mapper.mysql.FieldMapper;
import com.seven.base.mapper.mysql.TableMapper;
import com.seven.base.service.QueryServiceImpl;
import com.seven.swing.component.dialog.MyDialogBuilder;
import com.seven.swing.component.form.CreateForm;
import com.seven.swing.component.form.LoginForm;
import com.seven.swing.component.form.TemplateForm;
import com.seven.swing.component.panel.TablePanel;
import com.seven.swing.listener.AttachListenerFactory;
import com.seven.swing.model.EditTableModel;
import com.seven.swing.model.InitTableModel;
import com.seven.swing.model.ListTableModel;
import com.seven.swing.model.SearchTableModel;
import org.picocontainer.MutablePicoContainer;
import org.picocontainer.Parameter;
import org.picocontainer.defaults.BasicComponentParameter;
import org.picocontainer.defaults.DefaultPicoContainer;

/**
 * @author seven
 */
public class CodeAutoManager {

    public static Project PROJECT = null;
    private static CodeAutoManager codeAutoManager = new CodeAutoManager();
    private static MutablePicoContainer pico = new DefaultPicoContainer();

    /**
     * 初始化自己的容器池
     *
     * @return
     */
    public static CodeAutoManager instance() {

        // 注册action组件
        pico.registerComponentImplementation(CodeCreateAction.class);
        pico.registerComponentImplementation(TemplateConfigAction.class);


        // 注册数据库组件
        String queryServiceImplKey = "queryServiceImpl";
        pico.registerComponentImplementation(QueryDaoImpl.class);
        pico.registerComponentImplementation("mysqlFieldMapper", FieldMapper.class);
        pico.registerComponentImplementation("mysqlTableMapper", TableMapper.class);
        pico.registerComponentImplementation(queryServiceImplKey, QueryServiceImpl.class);


        // 注册model
        String editTableModelKey = "editTableModel";
        String initTableModelKey = "initTableModel";
        String listTableModelKey = "listTableModel";
        String searchTableModelKey = "searchTableModel";
        pico.registerComponentImplementation(editTableModelKey, EditTableModel.class, getParameters(queryServiceImplKey));
        pico.registerComponentImplementation(initTableModelKey, InitTableModel.class, getParameters(queryServiceImplKey));
        pico.registerComponentImplementation(listTableModelKey, ListTableModel.class, getParameters(queryServiceImplKey));
        pico.registerComponentImplementation(searchTableModelKey, SearchTableModel.class, getParameters(queryServiceImplKey));


        // 注册panel
        String editPanelKey = "editPanel";
        String initPanelKey = "initPanel";
        String listPanelKey = "listPanel";
        String searchPanelKey = "searchPanel";
        pico.registerComponentImplementation(editPanelKey, TablePanel.class, getParameters(editTableModelKey));
        pico.registerComponentImplementation(initPanelKey, TablePanel.class, getParameters(initTableModelKey));
        pico.registerComponentImplementation(listPanelKey, TablePanel.class, getParameters(listTableModelKey));
        pico.registerComponentImplementation(searchPanelKey, TablePanel.class, getParameters(searchTableModelKey));

        // 注册form
        String loginFormKey = "loginForm";
        pico.registerComponentImplementation(loginFormKey, LoginForm.class);

        String templateFormKey = "templateForm";
        pico.registerComponentImplementation(templateFormKey, TemplateForm.class);

        String createFormKey = "createForm";
        pico.registerComponentImplementation(createFormKey, CreateForm.class, getParameters(editPanelKey, initPanelKey, listPanelKey, searchPanelKey));

        // 注册事件
        String attachListenerFactoryKey = "attachListenerFactory";
        pico.registerComponentImplementation(attachListenerFactoryKey, AttachListenerFactory.class, getParameters(loginFormKey, createFormKey, templateFormKey, queryServiceImplKey));

        getBean(attachListenerFactoryKey);

        // 注册builder
        MyDialogBuilder.loginForm = getBean(loginFormKey);
        MyDialogBuilder.createForm = getBean(createFormKey);
        MyDialogBuilder.templateForm = getBean(templateFormKey);

        return codeAutoManager;
    }

    public static void dispose() {
        pico.dispose();
    }

    private static Parameter[] getParameters(String ... beanKeys) {
        Parameter[] parameters = new Parameter[beanKeys.length];
        for (int i = 0 ; i < beanKeys.length ; i++) {
            parameters[i] = new BasicComponentParameter(beanKeys[i]);
        }
        return parameters;
    }

    /**
     * 根据类从容器中取得组件
     *
     * @param clazz
     * @param <T>
     * @return
     */
    public static  <T> T getBean(Class<T> clazz) {
        return (T)pico.getComponentInstanceOfType(clazz);
    }

    /**
     * 根据存储的key
     * @param beanKey
     * @param <T>
     * @return
     */
    public static <T> T getBean(String beanKey) {
        return (T)pico.getComponentInstance(beanKey);
    }


}
