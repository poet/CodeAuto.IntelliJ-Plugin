package com.seven.swing;

import javax.swing.*;
import java.awt.*;

/**
 * 控件布局静态类
 * 要求组件的高度相同，如果不同直接调用JDK内部的方法
 * 
 * @author deyang
 *
 */
public class Layout {
	/**
	 *  X表示控件最左上侧点的X坐标
	 */
	public static int X = 60;
	
	/**
	 *  Y表示控件最左上侧点的Y坐标
	 */
	public static int Y = 40;
	
	/**
	 * 控件的高度
	 */
	public static int  H = 25;
	
	/**
	 * 控件的间隔距离
	 */
	public static int S = 5;
	
	/**
	 * JLabel控件默认的宽度
	 */
	public static int  W_JLABEL = 130;
	
	/**
	 * JTextField控件默认的宽度
	 */
	public static int  W_JTEXTFIELD = 200;
	
	/**
	 * JPasswordField控件默认的宽度
	 */
	public static int  W_JPASSWORDFIELD = 200;
	
	/**
	 * JComboBox控件默认的宽度
	 */
	public static int  W_JComboBox = 150;
	
	/**
	 * JRadioButton控件默认的宽度
	 */
	public static int  W_JRADIOBUTTON = 100;
	
	/**
	 * JButton控件默认的宽度
	 */
	public static int  W_JBUTTON = 80;
	
	/**
	 * JPanel控件默认的宽度
	 */
	public static int  W_JPANEL = 500;
	
	/**
	 * JPanel控件默认的高度
	 */
	public static int  H_JPANEL = 250;
	/**
	 * JScrollPane控件默认的高度
	 */
	public static int  H_JSCROLLPANE = 240;
	/**
	 * JScrollPane控件默认的宽度
	 */
	public static int  W_JSCROLLPANE = 650;
	
	/**
	 * JSplitPane控件默认的高度
	 */
	public static int H_JSPLITPANE = 240;
	
	/**
	 * JSplitPane控件默认的宽度
	 */
	public static int W_JSPLITPANE = 800;
	
	/**
	 * JTabbedPane控件默认的宽度
	 */
	public static int W_JTABBEDPANE = 800;
	
	/**
	 * JTabbedPane控件默认的高度
	 */
	public static int H_JTABBEDPANE = 300;
	
	/**
	 * JCheckBox控件默认的宽度
	 */
	public static int W_JCHECKBOX = 200;
	
	
	/**
	 * 真实的y轴位置
	 */
	private int realY = Y;
	
	/**
	 * 第几列
	 */
	private int realRow = 0;
	
	/**
	 * 上个组件的高度
	 */
	private int prevH = 0;
	
	/**
	 * 返回一个对象
	 * @return
	 */
	public static Layout ready(){
		return  new Layout();
	}
		
	/**
	 * 设置控件的位置
	 * 
	 * @param component 组件
	 * @param x x轴的坐标
	 * @param width 组件的宽度
	 * @param row 第几行
	 */
	public Layout setLocate(Component component,int x, int width, int row){
		int height = H;
		if(component instanceof JPanel){
			height = H_JPANEL;
		}
		if(component instanceof JScrollPane){
			height = H_JSCROLLPANE;
		}
		if(component instanceof JSplitPane){
			height = H_JSPLITPANE;
		}
		if(component instanceof JTabbedPane){
			height = H_JTABBEDPANE;
		}
		combRealY(height,row);
		component.setBounds(x, realY, width, height);
		return this;
	}

	/**
	 * 设置控件的位置,如果宽度没有指定则默认为组件的默认宽度
	 * 
	 * @param component 组件
	 * @param x x轴的坐标
	 * @param row 第几行
	 */
	public Layout setLocate(Component component,int x,int row){
		int width = 0;
		if(component instanceof JLabel){
			width = W_JLABEL;
		}else if(component instanceof JTextField){
			width = W_JTEXTFIELD;
		}else if(component instanceof JPasswordField){
			width = W_JPASSWORDFIELD;
		}else if(component instanceof JComboBox){
			width = W_JComboBox;
		}else if(component instanceof JRadioButton){
			width = W_JRADIOBUTTON;
		}else if(component instanceof JButton){
			width = W_JBUTTON;
		}else if(component instanceof JPanel){
			width = W_JPANEL;
		}else if(component instanceof JScrollPane){
			width = W_JSCROLLPANE;
		}else if(component instanceof JSplitPane){
			width = W_JSPLITPANE;
		}else if(component instanceof JCheckBox){
			width = W_JCHECKBOX;
		}else if(component instanceof JTabbedPane){
          width = W_JSPLITPANE;
        }else{
			System.out.println(component.getClass());
			throw new RuntimeException("没有定义的控件默认宽度错误");
		}
		return setLocate(component, x, width, row);
	}
	
	/**
	 * 设置控件的位置
	 * <br/>如果宽度没有指定则默认为组件的默认宽度
	 * <br/>如果X轴坐标没有指定则默认为<code>Config.X</code>
	 * 
	 * @param component 组件
	 * @param row 第几行
	 */
	public Layout setLocate(Component component,int row){
		return setLocate(component, X, row);
	}
	
	/**
	 * 整理y轴位置信息
	 * @param height 控件的高度
	 * @param row 行数 
	 */
	private void combRealY(int height, int row) {
		if(row == 0){
			this.prevH = height;
		}
		if(row != this.realRow){
			this.realY += (S + prevH);
			this.realRow = row;
			this.prevH = height;
		}
	}
}
