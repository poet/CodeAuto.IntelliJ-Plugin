package com.seven.swing.component.dialog;

import com.intellij.openapi.fileTypes.PlainTextFileType;
import com.intellij.openapi.ui.DialogBuilder;
import com.intellij.ui.EditorTextField;
import com.seven.base.util.FileUtil;
import com.seven.ioc.CodeAutoManager;
import com.seven.swing.component.form.CreateForm;
import com.seven.swing.component.form.LoginForm;
import com.seven.swing.component.form.TemplateForm;
import org.apache.commons.lang3.ArrayUtils;

import javax.swing.*;

/**
 * @author seven
 */
public class MyDialogBuilder {

    public static LoginForm loginForm;
    public static CreateForm createForm;
    public static TemplateForm templateForm;

    private static DialogBuilder loginBuilder;
    private static DialogBuilder createBuilder;
    private static DialogBuilder templateBuilder;

    public static void showLoginDialog() {
        loginBuilder = new DialogBuilder();
        loginBuilder.setCenterPanel(loginForm);
        loginBuilder.setDimensionServiceKey("CodeAuto.IntelliJ-Plugin.loginForm");
        loginBuilder.setTitle("登录数据库");
        loginBuilder.removeAllActions();
        loginBuilder.show();
    }

    public static void closeLoginDialog() {
        if (loginBuilder != null) {
            loginBuilder.getDialogWrapper().doCancelAction();
            loginBuilder = null;
        }
    }

    public static void showCreateDialog() {
        // 将当前的项目路径放在生成列表中
        createForm.getPathText().setText( CodeAutoManager.PROJECT.getBasePath());
        DefaultComboBoxModel model  = (DefaultComboBoxModel)createForm.getTemplateBox().getModel();
        model.removeAllElements();
        String[] templates = FileUtil.getTemplates();
        if (ArrayUtils.isNotEmpty(templates)) {
            for (String template : templates) {
                model.addElement(template);
            }
        }
        createForm.getTemplateBox().updateUI();

        createBuilder = new DialogBuilder();
        createBuilder.setCenterPanel(createForm);
        createBuilder.setDimensionServiceKey("CodeAuto.IntelliJ-Plugin.createForm");
        createBuilder.setTitle("生成模板代码");
        createBuilder.removeAllActions();
        createBuilder.show();
    }

    public static void closeCreateDialog() {
        if (createBuilder != null) {
            createBuilder.getDialogWrapper().doCancelAction();
            createBuilder = null;
        }
    }

    public static void showTemplateDialog() {

        // 初始化templateForm的编辑器
        EditorTextField textField = new EditorTextField("请点击左侧的文件进行修改", CodeAutoManager.PROJECT, PlainTextFileType.INSTANCE);
        textField.setOneLineMode(false);
        textField.setEnabled(true);
        textField.setAutoscrolls(true);
        textField.setSize(200,200);
        templateForm.setTemplateEditor(textField);

        templateBuilder = new DialogBuilder();
        templateBuilder.setCenterPanel(templateForm);
        templateBuilder.setDimensionServiceKey("CodeAuto.IntelliJ-Plugin.templateForm");
        templateBuilder.setTitle("模板配置");
        templateBuilder.removeAllActions();
        templateBuilder.show();
    }

    public static void closeTemplateDialog() {
        if (templateBuilder != null) {
            templateBuilder.getDialogWrapper().doCancelAction();
            templateBuilder = null;
        }
    }

}

