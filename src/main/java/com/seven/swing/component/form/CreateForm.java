/*
 * Created by JFormDesigner on Tue Sep 19 14:47:43 CST 2017
 */

package com.seven.swing.component.form;

import com.seven.base.bean.Assemble;
import com.seven.base.util.FileUtil;
import com.seven.swing.component.panel.TablePanel;
import org.apache.commons.lang3.StringUtils;

import java.awt.*;
import javax.swing.*;

/**
 * @author seven
 */
public class CreateForm extends JPanel {

    public CreateForm(TablePanel editPanel, TablePanel initPanel, TablePanel listPanel, TablePanel searchPanel) {
        this.editPanel = editPanel;
        this.searchPanel = searchPanel;
        this.listPanel = listPanel;
        this.initPanel = initPanel;
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - seven
        tabbedPanel = new JTabbedPane();
        panel1 = new JPanel();
        label1 = new JLabel();
        authorField = new JTextField();
        label2 = new JLabel();
        templateBox = new JComboBox(FileUtil.getTemplates());
        label3 = new JLabel();
        moduleText = new JTextField();
        label4 = new JLabel();
        pathText = new JTextField();
        sltButton = new JButton();
        createButton = new JButton();

        //======== this ========

        // JFormDesigner evaluation mark
//        setBorder(new javax.swing.border.CompoundBorder(
//            new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
//                "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
//                javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
//                java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

        setLayout(new GridBagLayout());
        ((GridBagLayout)getLayout()).columnWidths = new int[] {1000, 0};
        ((GridBagLayout)getLayout()).rowHeights = new int[] {300, 0, 0};
        ((GridBagLayout)getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)getLayout()).rowWeights = new double[] {1.0, 1.0, 1.0E-4};

        //======== tabbedPanel ========
        {
            tabbedPanel.addTab("请选择表", initPanel);
            tabbedPanel.addTab("信息编辑", editPanel);
            tabbedPanel.addTab("信息搜索", searchPanel);
            tabbedPanel.addTab("信息列表", listPanel);
        }
        add(tabbedPanel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));


        //======== panel1 ========
        {
            panel1.setLayout(new FlowLayout(FlowLayout.RIGHT));

            //---- label1 ----
            label1.setText("\u4f5c\u8005");
            panel1.add(label1);

            //---- authorField ----
            authorField.setMinimumSize(new Dimension(80, 24));
            authorField.setPreferredSize(new Dimension(80, 24));
            panel1.add(authorField);

            //---- label2 ----
            label2.setText("\u6a21\u677f");
            panel1.add(label2);
            panel1.add(templateBox);

            //---- label3 ----
            label3.setText("\u6a21\u5757");
            panel1.add(label3);

            //---- moduleText ----
            moduleText.setPreferredSize(new Dimension(80, 24));
            panel1.add(moduleText);

            //---- label4 ----
            label4.setText("\u8def\u5f84");
            panel1.add(label4);

            //---- pathText ----
            pathText.setPreferredSize(new Dimension(120, 24));
            panel1.add(pathText);

            //---- sltButton ----
            sltButton.setText("\u9009\u62e9");
            panel1.add(sltButton);

            //---- createButton ----
            createButton.setText("\u751f\u6210\u4ee3\u7801");
            panel1.add(createButton);
        }
        add(panel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - seven
    private TablePanel editPanel;
    private TablePanel searchPanel;
    private TablePanel listPanel;
    private TablePanel initPanel;
    private JTabbedPane tabbedPanel;

    private JPanel panel1;
    private JLabel label1;
    private JTextField authorField;
    private JLabel label2;
    private JComboBox templateBox;
    private JLabel label3;
    private JTextField moduleText;
    private JLabel label4;
    private JTextField pathText;
    private JButton sltButton;
    private JButton createButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


    public void initAssemble(Assemble ab) {
        authorField.setText(ab.getAuthor());
        moduleText.setText(ab.getModule());
        pathText.setText(ab.getCodepath());
        if (StringUtils.isNotBlank(ab.getTemplate())){
            templateBox.setSelectedItem(ab.getTemplate());
        }
    }

    public TablePanel getEditPanel() {
        return editPanel;
    }

    public void setEditPanel(TablePanel editPanel) {
        this.editPanel = editPanel;
    }

    public TablePanel getSearchPanel() {
        return searchPanel;
    }

    public void setSearchPanel(TablePanel searchPanel) {
        this.searchPanel = searchPanel;
    }

    public TablePanel getListPanel() {
        return listPanel;
    }

    public void setListPanel(TablePanel listPanel) {
        this.listPanel = listPanel;
    }

    public TablePanel getInitPanel() {
        return initPanel;
    }

    public void setInitPanel(TablePanel initPanel) {
        this.initPanel = initPanel;
    }

    public JTabbedPane getTabbedPanel() {
        return tabbedPanel;
    }

    public void setTabbedPanel(JTabbedPane tabbedPanel) {
        this.tabbedPanel = tabbedPanel;
    }

    public JTextField getAuthorField() {
        return authorField;
    }

    public void setAuthorField(JTextField authorField) {
        this.authorField = authorField;
    }

    public JComboBox getTemplateBox() {
        return templateBox;
    }

    public void setTemplateBox(JComboBox templateBox) {
        this.templateBox = templateBox;
    }

    public JTextField getModuleText() {
        return moduleText;
    }

    public void setModuleText(JTextField moduleText) {
        this.moduleText = moduleText;
    }

    public JTextField getPathText() {
        return pathText;
    }

    public void setPathText(JTextField pathText) {
        this.pathText = pathText;
    }

    public JButton getSltButton() {
        return sltButton;
    }

    public void setSltButton(JButton sltButton) {
        this.sltButton = sltButton;
    }

    public JButton getCreateButton() {
        return createButton;
    }

    public void setCreateButton(JButton createButton) {
        this.createButton = createButton;
    }
}
