/*
 * Created by JFormDesigner on Tue Sep 19 14:11:13 CST 2017
 */

package com.seven.swing.component.form;

import com.seven.base.bean.User;
import com.seven.base.util.Configuration;
import com.seven.base.util.FileUtil;

import java.awt.*;
import javax.swing.*;

/**
 * @author seven
 */
public class LoginForm extends JPanel {
    public LoginForm() {
        initComponents();
        initHasLoginInfo();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - seven
        this2 = new JPanel();
        label1 = new JLabel();
        dialogBox = new JComboBox(FileUtil.getDatabases());
        label2 = new JLabel();
        jdbcUrl = new JTextField();
        label3 = new JLabel();
        username = new JTextField();
        label4 = new JLabel();
        password = new JTextField();
        panel1 = new JPanel();
        loginButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setMinimumSize(new Dimension(357, 160));

        // JFormDesigner evaluation mark
//        setBorder(new javax.swing.border.CompoundBorder(
//            new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
//                "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
//                javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
//                java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

        setLayout(new GridBagLayout());
        ((GridBagLayout)getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)getLayout()).rowWeights = new double[] {1.0, 1.0, 1.0E-4};

        //======== this2 ========
        {
            this2.setLayout(new GridBagLayout());
            ((GridBagLayout)this2.getLayout()).columnWidths = new int[] {0, 300, 0};
            ((GridBagLayout)this2.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
            ((GridBagLayout)this2.getLayout()).columnWeights = new double[] {1.0, 1.0, 1.0E-4};
            ((GridBagLayout)this2.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- label1 ----
            label1.setText("\u6570\u636e\u5e93");
            this2.add(label1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            this2.add(dialogBox, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

            //---- label2 ----
            label2.setText("\u8fde\u63a5\u5730\u5740");
            this2.add(label2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            this2.add(jdbcUrl, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

            //---- label3 ----
            label3.setText("\u7528\u6237\u540d");
            this2.add(label3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            this2.add(username, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

            //---- label4 ----
            label4.setText("\u5bc6\u7801");
            this2.add(label4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 5), 0, 0));
            this2.add(password, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
        }
        add(this2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        //======== panel1 ========
        {
            panel1.setLayout(new FlowLayout(FlowLayout.RIGHT));

            //---- loginButton ----
            loginButton.setText("\u767b\u5f55");
            panel1.add(loginButton);

            //---- cancelButton ----
            cancelButton.setText("\u53d6\u6d88");
            panel1.add(cancelButton);
        }
        add(panel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - seven
    private JPanel this2;
    private JLabel label1;
    private JComboBox dialogBox;
    private JLabel label2;
    private JTextField jdbcUrl;
    private JLabel label3;
    private JTextField username;
    private JLabel label4;
    private JTextField password;
    private JPanel panel1;
    private JButton loginButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


    /**
     * 初始化面版
     */
    public void initHasLoginInfo() {
        User user = Configuration.getHasLoginUser();
        if (user != null) {
            this.dialogBox.setSelectedItem(user.getDialect());
            this.username.setText(user.getUsername());
            this.password.setText(user.getPassword());
            this.jdbcUrl.setText(user.getJdbcUrl());
        } else {
            this.jdbcUrl.setText("jdbc:mysql://192.168.1.241/{testDatabase}");
        }
    }

    /**
     * 取得登录的信息
     *
     * @return
     */
    public User getUser() {
        return new User(dialogBox.getSelectedItem().toString(), jdbcUrl.getText(), username.getText(), password.getText());
    }


    public JComboBox getDialogBox() {
        return dialogBox;
    }

    public void setDialogBox(JComboBox dialogBox) {
        this.dialogBox = dialogBox;
    }

    public JTextField getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(JTextField jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public JTextField getUsername() {
        return username;
    }

    public void setUsername(JTextField username) {
        this.username = username;
    }

    public JTextField getPassword() {
        return password;
    }

    public void setPassword(JTextField password) {
        this.password = password;
    }

    public JButton getLoginButton() {
        return loginButton;
    }

    public void setLoginButton(JButton loginButton) {
        this.loginButton = loginButton;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public void setCancelButton(JButton cancelButton) {
        this.cancelButton = cancelButton;
    }
}
