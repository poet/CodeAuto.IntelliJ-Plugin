/*
 * Created by JFormDesigner on Mon Sep 25 17:28:22 CST 2017
 */

package com.seven.swing.component.form;

import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.ui.EditorTextField;
import com.seven.base.util.Configuration;
import com.seven.base.util.FileUtil;
import org.springframework.util.CollectionUtils;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.io.File;
import java.util.List;

/**
 * @author seven
 */
public class TemplateForm extends JPanel {

    public TemplateForm() {
        initComponents();
        setMinimumSize(new Dimension(1000, 0));
        initTemplate();
    }

    private void initTemplate() {
        List<File> templateFiles = FileUtil.getHasTemplates();
        if (!CollectionUtils.isEmpty(templateFiles)) {
            for (File templateFile : templateFiles) {
                addTemplate(templateFile);
            }
        }
    }

    private JTree initDefalut() {
        DefaultMutableTreeNode root = new  DefaultMutableTreeNode ( "模板(0)" );
        return new JTree (root);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - seven
        scrollPane1 = new JScrollPane();
        templateTree = initDefalut();
        scrollPane2 = new JScrollPane();
        templateEditor = null;


        panel1 = new JPanel();
        importBtn = new JButton();
        exportBtn = new JButton();
        panel2 = new JPanel();
        cancelBtn = new JButton();
        ApplyBtn = new JButton();
        ApplyBtn.setEnabled(false);
        okBtn = new JButton();

        //======== this ========

        // JFormDesigner evaluation mark
//        setBorder(new javax.swing.border.CompoundBorder(
//            new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
//                "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
//                javax.swing.border.TitledBorder.BOTTOM, new Font("Dialog", Font.BOLD, 12),
//                Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

        setLayout(new GridBagLayout());
        ((GridBagLayout)getLayout()).columnWidths = new int[] {205, 0, 0};
        ((GridBagLayout)getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)getLayout()).rowWeights = new double[] {1.0, 0.0, 1.0E-4};

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(templateTree);
        }
        add(scrollPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //======== scrollPane2 ========
        {
            scrollPane2.setViewportView(templateEditor);
        }
        add(scrollPane2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        //======== panel1 ========
        {
            panel1.setLayout(new FlowLayout(FlowLayout.LEFT));

            //---- importBtn ----
            importBtn.setText("import");
            panel1.add(importBtn);

            //---- exportBtn ----
            exportBtn.setText("remove");
            panel1.add(exportBtn);
        }
        add(panel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

        //======== panel2 ========
        {
            panel2.setLayout(new FlowLayout(FlowLayout.RIGHT));

            //---- cancelBtn ----
            cancelBtn.setText("Cancel");
            panel2.add(cancelBtn);

            //---- ApplyBtn ----
            ApplyBtn.setText("Apply");
            panel2.add(ApplyBtn);

            //---- okBtn ----
            okBtn.setText("OK");
            panel2.add(okBtn);
        }
        add(panel2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - seven
    private JScrollPane scrollPane1;
    private JTree templateTree;
    private JScrollPane scrollPane2;
    private EditorTextField templateEditor;
    private JPanel panel1;
    private JButton importBtn;
    private JButton exportBtn;
    private JPanel panel2;
    private JButton cancelBtn;
    private JButton ApplyBtn;
    private JButton okBtn;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


    public EditorTextField getTemplateEditor() {
        return templateEditor;
    }

    public void setTemplateEditor(EditorTextField templateEditor) {
        this.templateEditor = templateEditor;
        this.scrollPane2.setViewportView(this.templateEditor);
    }

    public JTree getTemplateTree() {
        return templateTree;
    }

    public void setTemplateTree(JTree templateTree) {
        this.templateTree = templateTree;
    }

    public JButton getImportBtn() {
        return importBtn;
    }

    public void setImportBtn(JButton importBtn) {
        this.importBtn = importBtn;
    }

    public JButton getExportBtn() {
        return exportBtn;
    }

    public void setExportBtn(JButton exportBtn) {
        this.exportBtn = exportBtn;
    }

    public JButton getCancelBtn() {
        return cancelBtn;
    }

    public void setCancelBtn(JButton cancelBtn) {
        this.cancelBtn = cancelBtn;
    }

    public JButton getApplyBtn() {
        return ApplyBtn;
    }

    public void setApplyBtn(JButton applyBtn) {
        ApplyBtn = applyBtn;
    }

    public JButton getOkBtn() {
        return okBtn;
    }

    public void setOkBtn(JButton okBtn) {
        this.okBtn = okBtn;
    }

    public JScrollPane getScrollPane2() {
        return scrollPane2;
    }

    public void setScrollPane2(JScrollPane scrollPane2) {
        this.scrollPane2 = scrollPane2;
    }

    /**
     * 添加模板
     * @param templateFile
     */
    public void addTemplate(File templateFile) {
        VirtualFile virtualFile = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(templateFile);
        virtualFile.refresh(false, true);
        DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(virtualFile.getName());
        initChild(treeNode, virtualFile);
        DefaultMutableTreeNode root = (DefaultMutableTreeNode)templateTree.getModel().getRoot();

        root.add(treeNode);
        root.setUserObject(String.format("模板(%s)", root.getChildCount()));
        templateTree.updateUI();
        Configuration.saveTemplate(virtualFile.getName(), virtualFile.getPath());
    }

    private void initChild(DefaultMutableTreeNode treeNode, VirtualFile virtualFile) {
        if (virtualFile.isDirectory()) {
            DefaultMutableTreeNode childNode;
            String name;
            for (VirtualFile child : virtualFile.getChildren()) {
                name = child.getName();
                if (!name.startsWith(".")) {
                    childNode = new DefaultMutableTreeNode(name);
                    treeNode.add(childNode);
                    initChild(childNode, child);
                }
            }
        }
    }

    public void removeTemplate(DefaultMutableTreeNode selectionNode) {
        selectionNode.removeFromParent();

        DefaultMutableTreeNode root = (DefaultMutableTreeNode)templateTree.getModel().getRoot();
        root.setUserObject(String.format("模板(%s)", root.getChildCount()));
        templateTree.updateUI();
        Configuration.removeTemplate(selectionNode.toString());
    }
}
