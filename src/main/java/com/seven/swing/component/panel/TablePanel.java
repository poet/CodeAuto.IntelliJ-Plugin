package com.seven.swing.component.panel;

import com.seven.base.util.PropertyUtil;
import com.seven.swing.model.BaseTableModel;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 根据表格模型创建表格模板
 * 
 * @author deyang
 *
 */
@SuppressWarnings("serial")
public class TablePanel extends JPanel {

	private BaseTableModel model;
	private JTabbedPane tab = new JTabbedPane();
	private Map<String, JSplitPane> content = new HashMap<String, JSplitPane>();
	private Map<String, BaseTableModel> mapModel = new HashMap<String, BaseTableModel>();

	public TablePanel(BaseTableModel model) {
		// 布局
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] {0, 0};
		gridBagLayout.rowHeights = new int[] {0, 0};
		gridBagLayout.columnWeights = new double[] {1.0, 1.0E-4};
		gridBagLayout.rowWeights = new double[] {1.0, 1.0E-4};
		this.setLayout(gridBagLayout);

		// 创建组件
		this.model = model;

		// 将组件装入面板
		add(tab, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
				GridBagConstraints.CENTER, GridBagConstraints.BOTH,
				new Insets(0, 0, 0, 0), 0, 0));
	}
	
	/**
	 * 加载内容并添加监听器
	 * 
	 * @param tbNames 表名
	 */
	public void loadAndAddListener(List<String> tbNames){
		tab.removeAll();
		JSplitPane jsplit;

		// 1. 判断是否是表格初始化
		if (tbNames == null) {
			String dbName = PropertyUtil.get("db.name");
			jsplit = content.get(dbName);
			if (jsplit == null) {
				jsplit = model.createJSplit(null);
				content.put(dbName, jsplit);
				mapModel.put(dbName, model);
			}
			tab.addTab(dbName, jsplit);
			return;
		}

		// 2. 字段类型的编辑
		for (String tbName : tbNames) {
			jsplit = content.get(tbName);
			if (jsplit == null) {
				jsplit = model.createJSplit(tbName);
		    	content.put(tbName, jsplit);
		    	mapModel.put(tbName, (BaseTableModel)((JTable)((JScrollPane)jsplit.getRightComponent()).getViewport().getView()).getModel());
			}
			tab.addTab(tbName, jsplit);

			
		}
	}

	public BaseTableModel getModel() {
		return model;
	}

	public void setModel(BaseTableModel model) {
		this.model = model;
	}

	public JTabbedPane getTab() {
		return tab;
	}

	public void setTab(JTabbedPane tab) {
		this.tab = tab;
	}

	public Map<String, JSplitPane> getContent() {
		return content;
	}

	public void setContent(Map<String, JSplitPane> content) {
		this.content = content;
	}

	public Map<String, BaseTableModel> getMapModel() {
		return mapModel;
	}

	public void setMapModel(Map<String, BaseTableModel> mapModel) {
		this.mapModel = mapModel;
	}

}
