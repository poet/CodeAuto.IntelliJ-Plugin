package com.seven.swing.listener;

import com.intellij.openapi.ui.Messages;
import com.seven.base.bean.Assemble;
import com.seven.base.bean.User;
import com.seven.base.service.QueryService;
import com.seven.base.util.Configuration;
import com.seven.base.util.DataUtil;
import com.seven.base.util.FileUtil;
import com.seven.base.util.UserUtil;
import com.seven.swing.component.dialog.MyDialogBuilder;
import com.seven.swing.component.form.CreateForm;
import com.seven.swing.component.form.LoginForm;
import com.seven.swing.component.form.TemplateForm;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;

public class AttachListenerFactory {

	private LoginForm loginForm;
	private CreateForm createForm;
	private TemplateForm templateForm;
	private QueryService service;

	public AttachListenerFactory(LoginForm loginForm, CreateForm createForm, TemplateForm templateForm, QueryService service) {
		this.loginForm = loginForm;
		this.createForm = createForm;
		this.templateForm = templateForm;
		this.service = service;

		try {
			bindEvent();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void bindEvent() throws Exception {
		
		// 添加登录拦截器
		loginForm.getLoginButton().addActionListener(e -> {
			// 登录
			User user = loginForm.getUser();
			if (!user.isWhole()) {
				Messages.showInfoMessage("登录失败, 输入信息不完整~", "错误信息");
				return;
			}
			boolean bool = UserUtil.login(user);
			if (!bool) {
				Messages.showInfoMessage("登录失败, 请检查您的输入信息~", "错误信息");
				return;
			}
			// 保存登录信息
			Configuration.saveLoginUser(user);
			// 登录成功后隐藏登录窗口，显示代码创建窗口
			MyDialogBuilder.closeLoginDialog();
			createForm.getInitPanel().loadAndAddListener(null);
			createForm.initAssemble(Configuration.getHasAssemble());
			MyDialogBuilder.showCreateDialog();
		});
		loginForm.getCancelButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MyDialogBuilder.closeLoginDialog();
			}
		});
		
		// 选项卡监听器
		createForm.getTabbedPanel().addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				int seleced = createForm.getTabbedPanel().getSelectedIndex();
				if (seleced == 0) {
                    return;
                }
				// 取得选中的表的值
				List<String> tbNames = DataUtil.getSelectedTbNames(createForm.getInitPanel().getModel());
				switch (seleced) {
				case 1:
					createForm.getEditPanel().loadAndAddListener(tbNames);
					break;
				case 2:
					createForm.getSearchPanel().loadAndAddListener(tbNames);
					break;
				case 3:
					createForm.getListPanel().loadAndAddListener(tbNames);
					break;
				default:
					break;
				}
			}
		});
		
		// 选择路径监听器
		createForm.getSltButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				if(chooser.showOpenDialog(createForm) == JFileChooser.APPROVE_OPTION){
					createForm.getPathText().setText(chooser.getSelectedFile().getPath());
				}
			}
		});
		
		// 生成代码监听器
		createForm.getCreateButton().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// 数据验证
				String author = createForm.getAuthorField().getText();

				if (StringUtils.isBlank(author)) {
					Messages.showInfoMessage("作者不能为空~", "错误提示");
					return;
				}
				String module = createForm.getModuleText().getText();
				if (StringUtils.isBlank(module)) {
					Messages.showInfoMessage("所属模块不能为空~", "错误提示");
					return;
				}
				String path = createForm.getPathText().getText();
				if (StringUtils.isBlank(path)) {
					Messages.showInfoMessage("请选择生成路径~", "错误提示");
					return;
				}
				Object item = createForm.getTemplateBox().getSelectedItem();
				if (item == null) {
					Messages.showInfoMessage("模板没有配置~", "错误提示");
					return;
				}
				String template = item.toString();
				// 数据获取
				Assemble assemble = DataUtil.assemble(author, module, path,template, createForm);
				if (assemble.getItems().isEmpty()) {
					Messages.showInfoMessage("未配置任何信息~", "错误提示");
					return;
				}
				
				// 保存常用的配置
				Configuration.saveAssemble(assemble);
				
				// 代码生成
				try {
					service.createCode(assemble);
					// 成功提示
					Messages.showInfoMessage("代码已生成完毕！", "成功信息");
				} catch (Exception e2) {
					// 成功提示
					Messages.showInfoMessage("代码生成失败，请查看错误日志！", "错误信息");
				}
				
			}
		});

		// 选择树
		templateForm.getTemplateTree().addTreeSelectionListener(e -> {
			JTree tree = (JTree) e.getSource();
			DefaultMutableTreeNode selectionNode = (DefaultMutableTreeNode) tree
					.getLastSelectedPathComponent();
			if (selectionNode != null && selectionNode.isLeaf()) {
				templateForm.getTemplateEditor().setNewDocumentAndFileType(FileUtil.getFileType(selectionNode), FileUtil.getFileDocument(selectionNode));
			}

		});


		// 导入模板文件, 如果有相同的模板提示是否覆盖
		templateForm.getImportBtn().addActionListener(e -> {
			JFileChooser chooser = new JFileChooser();
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			if(chooser.showOpenDialog(templateForm) == JFileChooser.APPROVE_OPTION){
				File templateFile = chooser.getSelectedFile();

				String fileName = templateFile.getName();
				if (FileUtil.mapTemplate.containsKey(fileName)) {
					Messages.showInfoMessage("模板已存在！", "错误信息");
					return;
				}
				templateForm.addTemplate(templateFile);
				FileUtil.mapTemplate = Configuration.getHasTemplates();
			}
		});

		// 将模板删除
		templateForm.getExportBtn().addActionListener(e -> {
			DefaultMutableTreeNode selectionNode = (DefaultMutableTreeNode) templateForm.getTemplateTree()
					.getLastSelectedPathComponent();
			if (selectionNode == null || selectionNode.getLevel() != 1) {
                return;
            }

			templateForm.removeTemplate(selectionNode);
			FileUtil.mapTemplate = Configuration.getHasTemplates();
		});

		// Cancel
		templateForm.getCancelBtn().addActionListener(e -> {
			MyDialogBuilder.closeTemplateDialog();
		});

		// Apply
		templateForm.getApplyBtn().addActionListener(e -> {
			// 保存编辑内容
			FileUtil.saveChanges();
			templateForm.getApplyBtn().setEnabled(false);
		});

		// OK
		templateForm.getOkBtn().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FileUtil.saveChanges();
				MyDialogBuilder.closeTemplateDialog();
			}
		});



	}
	
}
