package com.seven.swing.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;

/**
 * 表格模型的基类
 * 
 * @author deyang
 *
 */
@SuppressWarnings("serial")
public class BaseTableModel extends DefaultTableModel {

	Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 构造方法
	 */
	public BaseTableModel(Object[] columnNames, int rowCount) {
		super(columnNames, rowCount);
	}

	/**
	 * 往表中添加多行数据
	 * 
	 * @param list 封装数据的对象集合
	 */
	public void addLotRow(List<Object[]> listRowData) {
		for (Object[] rowData : listRowData) {
			this.addRow(rowData);
		}
	}

	/**
	 * 设置表格的单元格是否可以编辑 <br>
	 * 这里设置列1与列2单元格内的内容不可编辑，如果不同，子类可以重载此方法
	 * 
	 * @param rowIndex 行号
	 * @param columnIndex 列号
	 * @return true表示可以统计，false表示不可编辑
	 */
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (columnIndex == 0 || columnIndex == 1) {
            return false;
        } else {
            return true;
        }
	}

	/**
	 * 判断要加入的数据与已经存在的老数据是否冲突，如果冲突则停止加入
	 * 
	 * @param rowData 要添加在数据
	 * @param columns 哪几列的内容是不能重复的
	 * @return true表示冲突
	 */
	protected boolean existed(Object[] rowData, int[] columns) {
		List<String> viewTbData = new ArrayList<String>();
		for (int row = 0; row < this.getRowCount(); row++) {
			viewTbData.add(getRowViewKey(columns, row));
		}
		return viewTbData.contains(getFurtherKey(columns, rowData));
	}
	
	/**
	 * 创建 JSplitPane 并添加对应的监听器
	 * 
	 * @param tbName 表名，如果为NULL表示全表
	 * @return JSplitPane 对象
	 */
	public JSplitPane createJSplit(String tbName){
		throw new RuntimeException(getClass().getName() + "请重写父类createJSplit()方法");
	}

	private Object getFurtherKey(int[] columns, Object[] rowData) {
		String str = "";
		for (int column : columns) {
			str += rowData[column];
		}
		return str;
	}
	private String getRowViewKey(int[] columns, int row) {
		String str = "";
		for (int column : columns) {
			str += this.getValueAt(row, column);
		}
		return str;
	}


}
