package com.seven.swing.model;

import com.seven.base.service.QueryService;
import com.seven.swing.listener.TBKeyAdapter;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

@SuppressWarnings("serial")
public class InitTableModel extends BaseTableModel{
	QueryService service;
	

	/** 列对应的标题数组 */
	private static Object[] COLUMN_NAMES = { "表名", "注释", "Java类型命名"};

	/** 构造方法 */
	public InitTableModel(QueryService service) {
		super(COLUMN_NAMES, 0);
		this.service = service;
	}
	
	/**
	 * 重载了DefaultTableModel的addRow方法
	 * 
	 * @param rowData 新添加的数据
	 */
	@Override
	public void addRow(Object[] rowData) {
		if (!existed(rowData, new int[] { 0 })) {
			super.addRow(rowData);
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if (columnIndex == 0) {
            return false;
        } else {
            return true;
        }
	}

	@Override
	public JSplitPane createJSplit(String tbName) {
		final BaseTableModel model = this;
		// 表选择监听器
		final JList<String> jlist = new JList<String>();
    	jlist.setListData(service.queryTbNames());
    	jlist.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting()){							
					String[] tbNames = jlist.getSelectedValuesList().toArray(new String[]{});
					model.addLotRow(service.tableRowDatas(tbNames));
					
				}
			}
		});
    	
    	// table删除,移动响应监听器
    	JTable table = new JTable(model);
    	table.addKeyListener(new TBKeyAdapter(table, model));

    	// 分割pane
    	JSplitPane jsplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,new JScrollPane(jlist), new JScrollPane(table));
    	jsplit.setDividerSize(4);
		jsplit.setDividerLocation(150);
		return jsplit;
	}
	
	

}
