package com.seven.swing.model;

import com.seven.base.service.QueryService;
import com.seven.swing.listener.TBKeyAdapter;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableColumn;

/**
 * 查询 - 表格模型
 * 
 * @author deyang
 *
 */
@SuppressWarnings("serial")
public class SearchTableModel extends BaseTableModel {
	
	QueryService service;
	
	/** 列对应的标题数组 */
	public final static Object[] COLUMN_NAMES = {"字段名", "类型", "说明",
			"查询条件" };

	/** 查询方式 */
	public static Object[] SEARCH_TYPES = { " = ", " != ", " like% ",
			" %like ", " %like% ", "between" };

	public SearchTableModel(QueryService service) {
		super(COLUMN_NAMES, 0);
		this.service = service;
	}

	/**
	 * 重载了DefaultTableModel的addRow方法
	 * 
	 * @param rowData
	 *            新添加的数据
	 */
	@Override
	public void addRow(Object[] rowData) {
		if (!existed(rowData, new int[] { 0 })) {
			super.addRow(rowData);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public JSplitPane createJSplit(final String tbName) {
		final BaseTableModel model = new SearchTableModel(service);
		// 表选择监听器
		final JList<String> jlist = new JList<String>();
    	jlist.setListData(service.queryFdNames(tbName));
    	jlist.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(e.getValueIsAdjusting()){							
					String[] fdNames = jlist.getSelectedValuesList().toArray(new String[]{});
					model.addLotRow(service.fieldSearcjRowDatas(tbName, fdNames));
					
				}
			}
		});
    	
    	// table删除,移动响应监听器
    	JTable table = new JTable(model);
    	table.addKeyListener(new TBKeyAdapter(table, model));
    	
    	// 控件丰富化
    	TableColumn valColumn = table.getColumn(COLUMN_NAMES[3]);
		valColumn.setCellEditor(new DefaultCellEditor(new JComboBox(
				SEARCH_TYPES)));

    	// 分割pane
    	JSplitPane jsplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,new JScrollPane(jlist), new JScrollPane(table));
    	jsplit.setDividerSize(4);
		jsplit.setDividerLocation(150);
		
		return jsplit;
	}
}
