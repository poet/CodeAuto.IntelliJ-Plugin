/*
 * Created by JFormDesigner on Tue Sep 19 14:47:43 CST 2017
 */

package form;

import javax.swing.*;
import java.awt.*;

/**
 * @author seven
 */
public class CreateForm extends JPanel {
    public CreateForm() {
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - seven
        tabbedPane1 = new JTabbedPane();
        panel_one = new JPanel();
        tab_one = new JTabbedPane();
        splitPane_one = new JSplitPane();
        scrollPane2 = new JScrollPane();
        list1 = new JList();
        scrollPane3 = new JScrollPane();
        table1 = new JTable();
        panel_two = new JPanel();
        tab_two = new JTabbedPane();
        scrollPane5 = new JScrollPane();
        splitPane4 = new JSplitPane();
        scrollPane8 = new JScrollPane();
        list3 = new JList();
        scrollPane9 = new JScrollPane();
        list5 = new JList();
        splitPane3 = new JSplitPane();
        scrollPane6 = new JScrollPane();
        list4 = new JList();
        scrollPane7 = new JScrollPane();
        table4 = new JTable();
        panel1 = new JPanel();
        label1 = new JLabel();
        authorField = new JTextField();
        label2 = new JLabel();
        templateBox = new JComboBox();
        label3 = new JLabel();
        moduleText = new JTextField();
        label4 = new JLabel();
        pathText = new JTextField();
        sltButton = new JButton();
        createButton = new JButton();

        //======== this ========

        // JFormDesigner evaluation mark
        setBorder(new javax.swing.border.CompoundBorder(
            new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

        setLayout(new GridBagLayout());
        ((GridBagLayout)getLayout()).columnWidths = new int[] {1000, 0};
        ((GridBagLayout)getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)getLayout()).rowWeights = new double[] {1.0, 1.0, 1.0E-4};

        //======== tabbedPane1 ========
        {

            //======== panel_one ========
            {
                panel_one.setLayout(new GridBagLayout());
                ((GridBagLayout)panel_one.getLayout()).columnWidths = new int[] {0, 0};
                ((GridBagLayout)panel_one.getLayout()).rowHeights = new int[] {200, 0};
                ((GridBagLayout)panel_one.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
                ((GridBagLayout)panel_one.getLayout()).rowWeights = new double[] {1.0, 1.0E-4};

                //======== tab_one ========
                {

                    //======== splitPane_one ========
                    {

                        //======== scrollPane2 ========
                        {
                            scrollPane2.setViewportView(list1);
                        }
                        splitPane_one.setLeftComponent(scrollPane2);

                        //======== scrollPane3 ========
                        {
                            scrollPane3.setViewportView(table1);
                        }
                        splitPane_one.setRightComponent(scrollPane3);
                    }
                    tab_one.addTab("tables", splitPane_one);
                }
                panel_one.add(tab_one, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            tabbedPane1.addTab("\u8bf7\u9009\u62e9\u8868", panel_one);

            //======== panel_two ========
            {
                panel_two.setLayout(new GridBagLayout());
                ((GridBagLayout)panel_two.getLayout()).columnWidths = new int[] {0, 0};
                ((GridBagLayout)panel_two.getLayout()).rowHeights = new int[] {0, 0};
                ((GridBagLayout)panel_two.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
                ((GridBagLayout)panel_two.getLayout()).rowWeights = new double[] {1.0, 1.0E-4};

                //======== tab_two ========
                {

                    //======== scrollPane5 ========
                    {

                        //======== splitPane4 ========
                        {

                            //======== scrollPane8 ========
                            {
                                scrollPane8.setViewportView(list3);
                            }
                            splitPane4.setLeftComponent(scrollPane8);

                            //======== scrollPane9 ========
                            {
                                scrollPane9.setViewportView(list5);
                            }
                            splitPane4.setRightComponent(scrollPane9);
                        }
                        scrollPane5.setViewportView(splitPane4);
                    }
                    tab_two.addTab("user", scrollPane5);

                    //======== splitPane3 ========
                    {

                        //======== scrollPane6 ========
                        {
                            scrollPane6.setViewportView(list4);
                        }
                        splitPane3.setLeftComponent(scrollPane6);

                        //======== scrollPane7 ========
                        {
                            scrollPane7.setViewportView(table4);
                        }
                        splitPane3.setRightComponent(scrollPane7);
                    }
                    tab_two.addTab("admin", splitPane3);
                }
                panel_two.add(tab_two, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            tabbedPane1.addTab("\u8bf7\u7f16\u8f91\u8868", panel_two);
        }
        add(tabbedPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        //======== panel1 ========
        {
            panel1.setLayout(new FlowLayout(FlowLayout.RIGHT));

            //---- label1 ----
            label1.setText("\u4f5c\u8005");
            panel1.add(label1);

            //---- authorField ----
            authorField.setMinimumSize(new Dimension(80, 24));
            authorField.setPreferredSize(new Dimension(80, 24));
            panel1.add(authorField);

            //---- label2 ----
            label2.setText("\u6a21\u677f");
            panel1.add(label2);
            panel1.add(templateBox);

            //---- label3 ----
            label3.setText("\u6a21\u5757");
            panel1.add(label3);

            //---- moduleText ----
            moduleText.setPreferredSize(new Dimension(80, 24));
            panel1.add(moduleText);

            //---- label4 ----
            label4.setText("\u8def\u5f84");
            panel1.add(label4);

            //---- pathText ----
            pathText.setPreferredSize(new Dimension(120, 24));
            panel1.add(pathText);

            //---- sltButton ----
            sltButton.setText("\u9009\u62e9");
            panel1.add(sltButton);

            //---- createButton ----
            createButton.setText("\u751f\u6210\u4ee3\u7801");
            panel1.add(createButton);
        }
        add(panel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - seven
    private JTabbedPane tabbedPane1;
    private JPanel panel_one;
    private JTabbedPane tab_one;
    private JSplitPane splitPane_one;
    private JScrollPane scrollPane2;
    private JList list1;
    private JScrollPane scrollPane3;
    private JTable table1;
    private JPanel panel_two;
    private JTabbedPane tab_two;
    private JScrollPane scrollPane5;
    private JSplitPane splitPane4;
    private JScrollPane scrollPane8;
    private JList list3;
    private JScrollPane scrollPane9;
    private JList list5;
    private JSplitPane splitPane3;
    private JScrollPane scrollPane6;
    private JList list4;
    private JScrollPane scrollPane7;
    private JTable table4;
    private JPanel panel1;
    private JLabel label1;
    private JTextField authorField;
    private JLabel label2;
    private JComboBox templateBox;
    private JLabel label3;
    private JTextField moduleText;
    private JLabel label4;
    private JTextField pathText;
    private JButton sltButton;
    private JButton createButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
