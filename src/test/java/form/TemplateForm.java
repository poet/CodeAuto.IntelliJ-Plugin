/*
 * Created by JFormDesigner on Mon Sep 25 17:28:22 CST 2017
 */

package form;

import java.awt.*;
import javax.swing.*;

/**
 * @author seven
 */
public class TemplateForm extends JPanel {
    public TemplateForm() {
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - seven
        scrollPane1 = new JScrollPane();
        templateTree = new JTree();
        scrollPane2 = new JScrollPane();
        templateEditor = new JEditorPane();
        panel1 = new JPanel();
        importBtn = new JButton();
        exportBtn = new JButton();
        panel2 = new JPanel();
        cancelBtn = new JButton();
        ApplyBtn = new JButton();
        okBtn = new JButton();

        //======== this ========
        setMinimumSize(new Dimension(1000, 69));

        // JFormDesigner evaluation mark
        setBorder(new javax.swing.border.CompoundBorder(
            new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

        setLayout(new GridBagLayout());
        ((GridBagLayout)getLayout()).columnWidths = new int[] {205, 600, 0};
        ((GridBagLayout)getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)getLayout()).rowWeights = new double[] {1.0, 0.0, 1.0E-4};

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(templateTree);
        }
        add(scrollPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        //======== scrollPane2 ========
        {

            //---- templateEditor ----
            templateEditor.setMinimumSize(new Dimension(410, 69));
            scrollPane2.setViewportView(templateEditor);
        }
        add(scrollPane2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        //======== panel1 ========
        {
            panel1.setLayout(new FlowLayout(FlowLayout.LEFT));

            //---- importBtn ----
            importBtn.setText("import");
            panel1.add(importBtn);

            //---- exportBtn ----
            exportBtn.setText("export");
            panel1.add(exportBtn);
        }
        add(panel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

        //======== panel2 ========
        {
            panel2.setLayout(new FlowLayout(FlowLayout.RIGHT));

            //---- cancelBtn ----
            cancelBtn.setText("Cancel");
            panel2.add(cancelBtn);

            //---- ApplyBtn ----
            ApplyBtn.setText("Apply");
            panel2.add(ApplyBtn);

            //---- okBtn ----
            okBtn.setText("OK");
            panel2.add(okBtn);
        }
        add(panel2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - seven
    private JScrollPane scrollPane1;
    private JTree templateTree;
    private JScrollPane scrollPane2;
    private JEditorPane templateEditor;
    private JPanel panel1;
    private JButton importBtn;
    private JButton exportBtn;
    private JPanel panel2;
    private JButton cancelBtn;
    private JButton ApplyBtn;
    private JButton okBtn;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
