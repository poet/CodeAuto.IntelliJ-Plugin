package ioc;

import com.intellij.util.pico.DefaultPicoContainer;
import ioc.entity.Apple;
import ioc.entity.Juicer;
import ioc.entity.Peelable;
import ioc.entity.Peeler;
import org.junit.Test;
import org.picocontainer.MutablePicoContainer;


/**
 * @author seven
 */
public class IocTest {

    @Test
    public void testIoc() throws Exception {

        MutablePicoContainer pico = new DefaultPicoContainer();
        pico.registerComponentImplementation("apple", Apple.class);
        pico.registerComponentImplementation(Juicer.class);
        pico.registerComponentImplementation(Peeler.class);


//        Peelable peelable = (Peelable) pico.getComponentInstanceOfType(Peelable.class);
//        System.out.println(peelable == null);
//        peelable.peel();

//        Peelable peelable = (Peelable)pico.getComponentInstance("apple");
//
//        System.out.println(peelable == null);
//        peelable.peel();


        Peeler juicer = (Peeler)pico.getComponentInstanceOfType(Peeler.class);
        juicer.start();
    }



    @Test
    public void testUserDir() {
        String usrHome = System.getProperty("user.home");
        System.out.println(usrHome);
    }

}
