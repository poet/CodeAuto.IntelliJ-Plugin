package jar;

import com.seven.base.util.PropertyUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;

/**
 * @author seven
 */
public class TestJar {

    @Test
    public void testJarFile() {
        String path = TestJar.class.getResource("/CodeAuto.IntelliJ-Plugin-1.0-SNAPSHOT.jar").getPath();

        try {
            JarFile jarFile = new JarFile(new File(path));

            Enumeration<JarEntry> entries = jarFile.entries();
            while (entries.hasMoreElements()) {
                JarEntry jarEntry = entries.nextElement();
                System.out.println(jarEntry.getName());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void testUrl() {
        String path = TestJar.class.getResource("/CodeAuto.IntelliJ-Plugin-1.0-SNAPSHOT.jar").getFile();

        System.out.println("path:" + path);

        URL url = null;
        try {
            url = new URL("jar", null, 0, "file:" + path + "!/database");
            URLConnection con = url.openConnection();
            if (con instanceof JarURLConnection) {
                JarURLConnection result = (JarURLConnection) con;
                JarInputStream jarInputStream = new JarInputStream(result.getInputStream());
                JarEntry entry;
                while ((entry = jarInputStream.getNextJarEntry()) != null) {
                    System.out.println(entry.getName());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void getTemplate() {
        String path = TestJar.class.getResource("/CodeAuto.IntelliJ-Plugin-1.0-SNAPSHOT.jar").getPath();

        try {
            JarFile jarFile = new JarFile(new File(path));

            String suffix = String.format("template/%s/", "xlxiot");
            String[] filters = new String [] {"java", "xml"};

            String fileName;
            Enumeration<JarEntry> entries = jarFile.entries();
            List<String> list = new ArrayList<>();
            while (entries.hasMoreElements()) {
                fileName = entries.nextElement().getName();
                if (fileName.startsWith(suffix) && StringUtils.endsWithAny(fileName, filters)) {
                    list.add(StringUtils.substringAfter(fileName, suffix));
                }
            }

            System.out.println(list.size());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }




}
