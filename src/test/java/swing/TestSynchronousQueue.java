package swing;

import org.junit.Test;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author seven
 */
public class TestSynchronousQueue {

    @Test
    public void test() {
        SynchronousQueue<Boolean> queue = getSynchronousQueue();
        try {
            Boolean b = queue.poll(1, TimeUnit.MILLISECONDS);
            if (b == null) {
                System.out.println("登录超时");
            }
        } catch (InterruptedException e) {

        }
    }

    public SynchronousQueue<Boolean> getSynchronousQueue() {
        final SynchronousQueue<Boolean> queue = new SynchronousQueue<>();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2);
                    queue.put(Boolean.TRUE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
        return queue;
    }
}
