package com.kuailework.module.${module}.bean;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

/**
 * 数据库表[${tb.name}]的数据模型
 * ${tb.comment}
 * <#if author??>
 * 
 * @author ${author}
 * </#if>
 */

@TableName("${tb.name}")
@SuppressWarnings("serial")
public class ${Entity} implements Serializable {

	@TableId
	private Long id;
    <#list tb.fds as fd>
    // ${fd.comment!fd.name} 
    @TableField("${fd.name}")
    private ${fd.javaType} ${fd.javaName};
    </#list>
    
    /** 默认构造函数 */
    public ${Entity}() {
    }
    
    /** 全参构造函数 */
    public ${Entity}(<#list tb.fds as fd>${fd.javaType} ${fd.javaName}<#if fd_has_next>, </#if></#list>) {
      <#list tb.fds as fd>
      this.${fd.javaName} = ${fd.javaName};
      </#list>
    }
     
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
    <#list tb.fds as fd>
    public  ${fd.javaType} get${fd.javaName?cap_first}() {
        return this.${fd.javaName};
    }
    public void set${fd.javaName?cap_first}(${fd.javaType} ${fd.javaName}) {
        this.${fd.javaName} = ${fd.javaName};
    }
    </#list>
}
