package com.kuailework.module.${module}.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.plugins.Page;
import com.kuailework.module.${module}.bean.${Entity};
import com.kuailework.module.${module}.service.${Entity}Service;

/**
 * ${tb.comment}的维护
 * 
 * @author ${author}
 *
 */
@RestController
public class ${Entity}Controller {

	@Autowired
	private ${Entity}Service ${entity}Service;

	// ${tb.comment}的列表
	@RequestMapping("/${entity}/list")
	public Page<${Entity}> list(int pageNum, int pageSize) {
		return ${entity}Service.selectPage(new Page<>(pageNum, pageSize));
	}

	// ${tb.comment}的编辑
	@RequestMapping("/${entity}/edit")
	public void edit(${Entity} record) {
		${entity}Service.insertOrUpdate(record);
	}

	// ${tb.comment}的详情
	@RequestMapping("/${entity}/get")
	public ${Entity} detail(Long id) {
		return ${entity}Service.selectById(id);
	}

	// ${tb.comment}的删除
	@RequestMapping("/${entity}/remove")
	public void remove(Long id) {
		${entity}Service.deleteById(id);
	}
	
}
