package com.kuailework.module.${module}.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.kuailework.module.${module}.bean.${Entity};
/**
 * 
 * @author ${author}
 *
 */
public interface ${Entity}Mapper extends BaseMapper<${Entity}>{

}
