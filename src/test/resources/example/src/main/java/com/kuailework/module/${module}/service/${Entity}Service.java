package com.kuailework.module.${module}.service;

import com.baomidou.mybatisplus.service.IService;
import com.kuailework.module.${module}.bean.${Entity};

/**
 * 
 * @author ${author}
 *
 */
public interface ${Entity}Service extends IService<${Entity}>{

}
