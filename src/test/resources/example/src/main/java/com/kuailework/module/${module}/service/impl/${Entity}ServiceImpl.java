package com.kuailework.module.${module}.service.impl;

import org.springframework.stereotype.Service;

import com.kuailework.core.base.BaseServiceImpl;
import com.kuailework.module.${module}.bean.${Entity};
import com.kuailework.module.${module}.mapper.${Entity}Mapper;
import com.kuailework.module.${module}.service.${Entity}Service;

/**
 * 
 * @author ${author}
 *
 */
@Service
public class ${Entity}ServiceImpl extends BaseServiceImpl<${Entity}Mapper, ${Entity}> implements ${Entity}Service {

	// 方法里面使用 baseMapper 调用
}
