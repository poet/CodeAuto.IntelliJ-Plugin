package ${module};

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.kuailework.Application;
import com.kuailework.core.util.JsonFormat;

/**
 * ${Entity}Controller集成测试
 * 
 * @author ${author}
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment=WebEnvironment.MOCK)
public class ${Entity}ControllerTest {

	@Autowired
    private WebApplicationContext context;

    private MockMvc mockMvc;
    

    @Before
    public void setupMockMvc() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    // 测试取得${tb.comment}的列表信息
	@Test
	public void testList() throws Exception {
		String jsonStr = mockMvc.perform(get("/${entity}/list").param("pageNum", "1").param("pageSize", "2").accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn().getResponse().getContentAsString();
		JsonFormat.out(jsonStr);
	}
	
	// 测试取得${tb.comment}的保存
	@Test
	public void testSave() throws Exception {
		MultiValueMap<String, String> env = new LinkedMultiValueMap<>();
		env.add("key", "");
		String jsonStr = mockMvc.perform(get("/${entity}/edit").params(env).accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn().getResponse().getContentAsString();
		JsonFormat.out(jsonStr);
	}
	
	// 测试取得${tb.comment}的更新
	@Test
	public void testUpdate() throws Exception {
		MultiValueMap<String, String> env = new LinkedMultiValueMap<>();
		env.add("id", "11");
		env.add("key", "");
		String jsonStr = mockMvc.perform(get("/${entity}/edit").params(env).accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn().getResponse().getContentAsString();
		JsonFormat.out(jsonStr);
	}
	
	// 测试取得${tb.comment}的详情信息
	@Test
	public void testGet() throws Exception {
		String jsonStr = mockMvc.perform(get("/${entity}/get").param("id", "11").accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn().getResponse().getContentAsString();
		JsonFormat.out(jsonStr);
	}
	
	// 测试取得${tb.comment}的删除
	@Test
	public void testRemove() throws Exception {
		String jsonStr = mockMvc.perform(get("/${entity}/remove").param("id", "11").accept(MediaType.APPLICATION_JSON_UTF8))
				.andReturn().getResponse().getContentAsString();
		JsonFormat.out(jsonStr);
	}
	

}