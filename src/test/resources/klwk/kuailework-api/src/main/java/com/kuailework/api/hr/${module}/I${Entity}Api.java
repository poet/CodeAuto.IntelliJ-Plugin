package com.kuailework.api.hr.${module};

import com.kuailework.api.hr.${module}.model.${Entity}Model;

public interface I${Entity}Api {

    ${Entity}Model get(Long id);

}
