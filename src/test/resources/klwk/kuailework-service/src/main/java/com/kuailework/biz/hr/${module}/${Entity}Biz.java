package com.kuailework.biz.hr.${module};

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuailework.api.hr.${module}.I${Entity}Api;
import com.kuailework.api.hr.${module}.model.${Entity}Model;
import com.kuailework.common.util.ConvertTool;
import com.kuailework.service.application.hr.${module}.I${Entity}Service;
import com.kuailework.service.application.hr.${module}.model.dto.${Entity}DTO;

@Service
public class ${Entity}Biz implements I${Entity}Api{

    @Autowired
    I${Entity}Service ${entity}Service;
    
    @Override
    public ${Entity}Model get(Long id) {
        ${Entity}DTO dto = ${entity}Service.get(id);
        ${Entity}Model model = new ${Entity}Model();
        ConvertTool.convert(dto, model);
        return model;
    }

}
