package com.kuailework.service.application.hr.${module};

import com.kuailework.service.application.hr.${module}.model.dto.${Entity}DTO;

public interface I${Entity}Service {

    ${Entity}DTO get(Long id);

}
