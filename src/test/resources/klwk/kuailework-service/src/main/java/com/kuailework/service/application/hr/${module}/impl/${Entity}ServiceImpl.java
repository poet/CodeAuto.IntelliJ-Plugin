package com.kuailework.service.application.hr.${module}.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuailework.common.util.ConvertTool;
import com.kuailework.dao.company.custom.${Entity}WriteMapper;
import com.kuailework.model.pojo.${Entity};
import com.kuailework.service.application.hr.${module}.I${Entity}Service;
import com.kuailework.service.application.hr.${module}.model.dto.${Entity}DTO;

@Service
public class ${Entity}ServiceImpl implements I${Entity}Service {
    
    @Autowired
    ${Entity}WriteMapper ${entity}WriteMapper;

    @Override
    public ${Entity}DTO get(Long id) {
        ${Entity} ${entity} = ${entity}WriteMapper.selectByPrimaryKey(id);
        ${Entity}DTO dto = new ${Entity}DTO();
        ConvertTool.convert(${entity}, dto);
        return dto;
    }

}
