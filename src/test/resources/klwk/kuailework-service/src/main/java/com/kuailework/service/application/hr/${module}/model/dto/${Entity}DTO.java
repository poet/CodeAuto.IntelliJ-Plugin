package com.kuailework.service.application.hr.${module}.model.dto;

import java.io.Serializable;
/**
 * 数据库表[${tb.name}]的数据模型
 * ${tb.comment}
 * <#if author??>
 * 
 * @author ${author}
 * </#if>
 */

@SuppressWarnings("serial")
public class ${Entity}DTO implements Serializable {

    <#list tb.fds as fd>
    // ${fd.comment!fd.name} 
    private ${fd.javaType} ${fd.javaName};
    </#list>
    
    /** 默认构造函数 */
    public ${Entity}DTO() {
    }
    
    /** 全参构造函数 */
    public ${Entity}DTO(<#list tb.fds as fd>${fd.javaType} ${fd.javaName}<#if fd_has_next>, </#if></#list>) {
      <#list tb.fds as fd>
      this.${fd.javaName} = ${fd.javaName};
      </#list>
    }
     
    <#list tb.fds as fd>
    public  ${fd.javaType} get${fd.javaName?cap_first}() {
        return this.${fd.javaName};
    }
    public void set${fd.javaName?cap_first}(${fd.javaType} ${fd.javaName}) {
        this.${fd.javaName} = ${fd.javaName};
    }
    </#list>
}



