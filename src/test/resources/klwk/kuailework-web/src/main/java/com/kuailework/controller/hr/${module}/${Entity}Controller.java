package com.kuailework.controller.hr.${module};

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kuailework.api.hr.${module}.I${Entity}Api;
import com.kuailework.api.hr.${module}.model.${Entity}Model;
import com.kuailework.web.annotation.Security;
import com.kuailework.web.vo.ResponseEntity;
import com.kuailework.web.vo.StatusInfo;

<#if author??>
/**
 * 
 * @author ${author}
 *
 */
</#if>
@Controller
@RequestMapping("/${module}/${entity}")
public class ${Entity}Controller {
    
    private static final Logger log = LoggerFactory.getLogger(${Entity}Controller.class);
    
    @Autowired
    private I${Entity}Api ${entity}Api;
    
    // 自动生成的demo示例
    @RequestMapping("/detail.json")
    @Security(mustLogin = true)
    @ResponseBody
    public ResponseEntity<?> demo(HttpServletRequest request, Long id) {
        try {
            ${Entity}Model model = ${entity}Api.get(id);
            return new ResponseEntity<Object>(StatusInfo.REQUEST_SUCCESS, StatusInfo.REQUEST_SUCCESS_MSG, model);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<Void>(StatusInfo.REQUEST_FAILURE, StatusInfo.REQUEST_FAILURE_MSG);
        }
    }
    
    
    
    
    
    
    
    
    
}
